@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Invoice Header</div>
                <div class="panel-body">
                    <h1>this is a print bridge page. you will be redirected in <span id="seconds">15</span> seconds.</h1>
                    <h2>If you don't see invoice, please enable pop-up on this URL from pop-up alert box in address box.</h2>
                    <button type="button" clas="btn btn-primary">Redirect Back</button>
                </div>
            </div>
        </div>
    </div>
</div>
        


@endsection

@section('script')
<script type="text/javascript">
    /*global $*/
    $(function(){
        window.open("{{$print_url}}","_blank");
        
        $("button").on("click", function(){
           window.location = "{{route('sales')}}";
        });
        
        var myVar = setInterval(function() { 
            var seconds = document.getElementById("seconds").innerHTML;
            seconds = parseInt (seconds);
            if (seconds <= 0)
                myStopFunction();
            else {
              seconds = seconds - 1;
              document.getElementById("seconds").innerHTML = seconds;
              
            }
        }, 1000);

        function myStopFunction() {
            clearInterval(myVar);
            window.location.href="{{route('sales')}}";
        }
    });
</script>
@endsection