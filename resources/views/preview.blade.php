<!DOCTYPE html>
<html>
    <head>
        <title>Invoice</title>
        <link rel="stylesheet" href="{{ asset('css/print.css') }}" type="text/css" />
    </head>
    <body>
    	<a href="{{route('sales')}}" class="btn btn-default">Back To Sales Invoice</a>
        <div class="invoice-header-main">
		
			<ul id="invoice-legend">
				<li>{{$version_string[0]}}</li>
				<li style="visibility:hidden">White: Duplicate for Transport</li>
				
			</ul>
	
			<h4 id="top-heading">TAX INVOICE</h4>
			
			<div id="images" align="center">
				<img src="{{ asset('images/ahvn.png') }}" class="logo" style="float:left;margin-left: -8em;" >
				<img src="{{ asset('images/mb_logo.png') }}" class="logo" style="float:right;margin-right: 2em;">
			</div>
			
			<div style="margin-top:4em">
				<span style="float:left;text-align:center;font-size:14px; margin-top:5px; padding-left:3px;">
					156-A, Ganesh Peth, No. 4-5, Kothari Plaza, Pangulali Chowk, Pune-2<br>
					Email: huzaifakoicha@ymail.com Tel: 020-64002183
				</span>
				<span style="float:right; font-size: 14px;text-align:center; font-weight: 900;padding-right:10px; margin-top:5px;">
					WHOLESALERS OF :-<br>
					ALL KINDS OF BRASS & <br>
					FURNITURE HARDWARE
				</span>
			</div>
        </div>
		
		
        <div id="wrapper">
            <div id="left">
                <div style="margin-top:3px;">
					<span>Name: </span>
					<input type="text" name="name_box" id="name_box" class="name_box" value="{{ $invoice_header->buyer->name }}" readonly>
				</div>
				<div style="margin-top:3px;">
					<span>Address: </span>
					<input type="text" name="address_box_1" id="address_box_1" class="address_box_1" value="{{ $invoice_header->buyer->address }}" readonly>
					<input type="text" name="address_box_2" id="address_box_2" class="address_box_2" value="{{ $invoice_header->buyer->address_2 }}. {{ $invoice_header->buyer->mobile }} " readonly>
				</div>
				
				
				<div style="margin-top:3px;">
					<span>State: </span>
					<input type="text" name="state_box" id="state_box" class="state_box" value="{{ $invoice_header->buyer->state}}" readonly>
				</div>
				
				<div style="margin-top:3px;">
					<span>GSTN: </span>
					<input type="text" name="gstn_box" id="gstn_box" class="gstn_box"  value="{{ $invoice_header->buyer->gstn }}" readonly>
				</div>
			</div>

            <div id="right">
                <div style="margin-top:5px;">
					<span>INVOICE NUMBER: </span>
					<input type="text" name="inv_num_box" id="inv_num_box" class="inv_num_box"  value="{{ $invoice_header->invoice_number }}" readonly>
				</div>
				
				<div style="margin-top:5px;">
					<span>INVOICE DATE: </span>
					<?php
						$date = DateTime::createFromFormat('Y-m-d', $invoice_header->invoice_date);
					?>
					<input type="text" name="inv_date_box" id="inv_date_box" class="inv_date_box"  value="{{ $date->format('d/m/Y') }}" readonly>
				</div>
				
				<div style="margin-top:5px;">
					<span>TRANSPORT: </span>
					<input type="text" name="transport_box" id="transport_box" class="transport_box" value="{{ $invoice_header->transport['transport_name'] }}" readonly>
				</div>
				
				<div style="margin-top:5px;">
					<span>CHALLAN NO.: </span>
					<input type="text" name="challan_no_box" id="challan_no_box" class="challan_no_box" value="{{ $invoice_header->challan_number }}" readonly>
				</div>
				
            </div>
        </div>
		<div class="invoice-lines">
			<table border="1" id="invoice_lines_tbl" style="background-image: url({{asset('images/mb_watermark.png')}}); background-repeat:no-repeat; background-position: center;background-size: 680px 388px;">
				<thead>
					<tr>
						<th>
							No.
						</th>
						<th style="width: 42%">
							Description
						</th>
						<th>
							HSN<br>CODE
						</th>
						<th style="width: 8%">
							Qty.
						</th>
						<th>Unit</th>
						<th>
							RATE
						</th>
						<th  style="width: 6%">
							LESS<br><span style="font-size: 7px;">DISCOUNT</span>
						</th>
						<th style="width: 20%" colspan="2">
							Amount
						</th>
						
					</tr>
				</thead>
				<tbody>
					
					@foreach ($invoice_header->invoice_lines as $key => $inv_line)
					<tr>
						<td id="no">{{++$key}}</td>
						<td class="description">{{$inv_line->item->description}}</td>
						<td>{{$inv_line->item->hsn_code}}</td>
						<td>{{$inv_line->quantity}}</td>
						<td>{{$inv_line->unit->short_name}}</td>
						
						<td>{{$inv_line->rate}}</td>
						
						@if (!is_null($inv_line->discount))
							<td>{{$inv_line->discount}}%</td>
						@else
							<td></td>
						@endif
						
						<?php
							$dig_arr = explode(".", $inv_line->amount);
						?>
						
						<td class="amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
					@endforeach
					
					<tr>
						<td colspan="5" rowspan="2">
							<div id="gst">GST TIN No.: 27ACPPM1031Q1Z6<br><div>
							<div id="vat_cst">VAT TIN : 27180769738 V w.e.f. - 1-4-10<br>
							CST TIN : 27180769738 C w.e.f. - 1-4-10<div>
						</td>
						<td colspan="2" class="total_tax tax_name description">TOTAL</td>
						<?php
							$dig_arr = explode(".", $invoice_header->invoice_total);
						?>
						<td class="total_tax amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
						
					</tr>
					<tr>
						<td colspan="2" class="total_tax tax_name description">CGST @ @if(!empty($invoice_header->cgst_percent)) {{$invoice_header->cgst_percent}}% @else {{ '  -' }} @endif</td>
						<?php
							$dig_arr = explode(".", $invoice_header->cgst_amount);
						?>
						<td class="total_tax amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
					<tr>
						<td colspan="5" rowspan="2" style="padding-left: 10px;vertical-align:top" id="amount_in_words" class="description uppercase">&#8377;. <span style="font-weight: normal; font-size: 14px;">{{ $invoice_header->amount_in_words }}</span></td>
						<td colspan="2" class="total_tax tax_name description">SGST @ @if(!empty($invoice_header->sgst_percent)) {{$invoice_header->sgst_percent}}% @else {{ '  -' }} @endif</td>
						<?php
							$dig_arr = explode(".", $invoice_header->sgst_amount);
						?>
						<td class="total_tax amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
					<tr>
						<td colspan="2" class="total_tax tax_name description">IGST @ @if(!empty($invoice_header->igst_percent)) {{$invoice_header->igst_percent}}% @else {{ '  -' }} @endif</td>
						<?php
							$dig_arr = explode(".", $invoice_header->igst_amount);
						?>
						<td class="total_tax amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
					<tr>
						<td colspan="5" rowspan="2" id="account_details"  class="description">
							ACCOUNT NAME: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AL HADEED VAN NOHAAS TRADERS<br>
							ACCOUNT NUMBER:   &nbsp;&nbsp;&nbsp;06332560002012<br>
							BANK NAME:   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HDFC BANK<br>
							IFSC CODE:   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HDFC0000633<br>
							BANK BRANCH:   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NANAPETH, PUNE (MAHARASHTRA).
						</td>
						<td colspan="2" class="total_tax tax_name description">ROUND OFF</td>
						<?php
							$dig_arr = explode(".", $invoice_header->round_off);
						?>
						<td class="total_tax amount">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
					<tr>
						<td colspan="2" class="total_tax tax_name description">GRAND TOTAL</td>
						<?php
							$dig_arr = explode(".", $invoice_header->grand_total);
						?>
						<td class="total_tax amount grand_total">@if (!empty($dig_arr[0])) {{ $dig_arr[0] }} @else {{ 00 }} @endif</td>
						<td id="paisa" class="grand_total">@if (!empty($dig_arr[1])) {{ sprintf("%02d", $dig_arr[1]) }} @else {{ sprintf("%02d", 0) }} @endif</td>
					</tr>
				</tbody>
			</table>
			</table>
		</div>
		<div class="invoice_footer">
            <div id="left">
                <p style="text-align: justify">I/We here by certify that my/our registration certificate under the Maharashtra Value Added Tax Act 2006 is in force on the date on which the sales of the goods specified in this TAX INVOICE is made by me/us and that transaction of sales convered by this Tax Invoice has been effected by me/us and it shall be accounted for in the turn over of sales while filling of return and the due tax if any payable the sale has been paid or shall be paid.</p>
				Terms: <ol>
					<li>All Goods are sold Subject to Pune Jurisdiction.</li>
					<li>Goods once sold will not be taken back.</li>
					<li>Payment <strong>strictly<strong> within 15 days.</li>
				</ol>
            </div>
            <div id="right">
				<div>
					
					<p>For,&nbsp;<img class="top" src="{{ asset('images/ahvn.png') }}" alt="Al-Hadeed Van-Nohaas" /></p><br><br><br><br><br>
					<div align="center">Auth. Signatory</div>
				</div>
            </div>
        </div>
    
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script type="text/javascript">
        $(function(){
          var rowCount = $("#invoice_lines_tbl tr").length;
          rowCount = parseInt(rowCount) - 7;
          totalCount = 18;
          remainingCount = totalCount - rowCount;
          
          for (var i = 0; i < remainingCount; i++) {
          	$("#invoice_lines_tbl > tbody > tr").eq(rowCount + i - 1).after("<tr><td id='no'>"+(rowCount + i + 1)+"</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td id='paisa'></tr>");
          }
          
          //window.print(); 
        });
    </script>
</html>