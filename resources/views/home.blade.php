@extends('layouts.app')

@section('pagecss')

<style>

</style>
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">-->
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                Welcome, {{$msg}}!<br><br>
                <div class="row table_header" style="margin-top: 3px;">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="no_of_record">No. of Records: </label>
                                <select name="no_of_record" id="no_of_record" class="form-control">
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="500">500</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4">
                        <div class="form-group">
                            <div class="col-md-12" style="margin-top: 50px">
                                <button type="button" name="download-report" class="btn waves-effect waves-light"><i class="glyphicon glyphicon-download-alt"></i> Download Report</button>
                                <!--<button type="button" name="pdf" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-export"></i> PDF</button>-->
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label for="search">Search terms: </label>
                                <input type="text" name="search" id="search" class="form-control input-sm" placeholder="Search" style="margin-top: 0"/>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row fadeIn" id="date-range-box" style="display: none;">
                    <div class="col-lg-4 col-lg-offset-3" style="margin-bottom:25px;padding-left: 30px;">
                        
                        <div class="col-lg-12" style="text-align:left">
                            <label><input type="checkbox" name="filetype[]" value='excel' id="excel" checked="checked"/>&nbsp;Excel</label><br>
                            <label><input type="checkbox" name="filetype[]" value='pdf' id="pdf"/>&nbsp;PDF</label>
                        </div>
                        
                        <div class="col-lg-12" style="text-align:left; margin-top:10px;margin-bottom:10px">
                            <label><input type="checkbox" name="include_lines" value='include_lines' id="include_lines" checked="checked"/>&nbsp;Include Invoice Lines</label><br>
                        </div>
                        
                        
                        <div class="col-lg-6">
                            <label for="from">From</label>
                        <input type="text" id="from" name="invoice_start_date" class="form-control">
                        </div>
                        
                        <div class="col-lg-6">
                            <label for="to">to</label>
                            <input type="text" id="to" name="invoice_end_date" class="form-control">    
                        </div>
                        
                        
                        <div class="col-lg-12">
                            
                                    <ul id="download-error-list" style="color:red"></ul>
                            
                        </div>
                        
                        <div align="center" style="">
                            <button type="button" name="download" class="btn btn-success btn-sm" style="margin-top: 10px;">Download</button>
                        </div>
                    </div>
                </div>
                
                <div class="dashboard-view">
                    {!! $dashboard !!}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script type="text/javascript">
    function showProcessing(processingText="Please wait...") {
        $("#loader_text").text(processingText);
        $("#processing_image.overlay").css({"visibility" :"visible", "opacity" :"1"});
    }
    
    function stopProcessing() {
        $("#processing_image.overlay").css({"visibility" :"hidden", "opacity" :"0"});
    }
</script>

<script type="text/javascript">
/*global $*/
function filter(column, orderby) {
    showProcessing("Ordering, wait...");
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('sort_table') }}",
        data: {
            "is_ajax":      true,
            "column":       column,
            "orderby":      orderby,
            "no_of_record": $("#no_of_record").val(),
            "search":       $("#search").val()
        },
        method: "POST",
        dataType: "json",
        complete: function(response) {
            stopProcessing();
        },
        success: function(response) {
            $(".dashboard-view").empty();
            $(".dashboard-view").html(response.html);
            
            if(orderby == "desc") {
                $("th[data-column='"+column+"']").data("orderby", "asc");
            } else {
                $("th[data-column='"+column+"']").data("orderby", "desc");
            }
        },
        failure: function(response) {}
            
    });
}

$(function(){
    $("button[data-invoice-number]").on("click", function(){
        if ($(this).data("btn-type") == "print")
            window.open("{{url('print')}}/"+$(this).data("invoice-number"), "_blank");
        else if ($(this).data("btn-type") == "printview")
            window.open("{{url('printview')}}/"+$(this).data("invoice-number"), "_blank");
        else if ($(this).data("btn-type") == "editinvoice") 
            window.location.href = $(this).data("url");
    });
    
    $(document).on("click", ".clickable", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("keyup", "#search", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("change", "#no_of_record", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $("button[name='download-report']").on("click", function(){
        $("#date-range-box").toggle();
    });
    
    $("button[name='download']").on("click", function() {
        if($("ul[id='download-error-list'] li").length <= 0) {
            //send request to download report as per required params....
            var excel = false;
            var pdf = false;
            
            if($("#excel").is(":checked"))
                excel = true;
            if($("#pdf").is(":checked"))
                pdf = true;
            
            window.open("{{route('download')}}"+"?invoice_start_date="+$("#from").val()+"&invoice_end_date="+$("#to").val()+"&excel="+excel+"&pdf="+pdf, "_blank");
        }
    });
    
    $("[name='filetype[]']").on("change", function() {
        if ($("[name='filetype[]']:checked").length <= 0)  {
            $("#download-error-list").append("<li id='filetype-error'>Please select at least one file type to download.</li>");
        } else {
            $("li[id='filetype-error']").remove();
        }
    });
    
    $("#from").on("change", function() {
        if($("#from").val() == "") {
            $("#download-error-list").append("<li id='from-error'>Please select a start date for report.</li>");
        } else {
            $("li[id='from-error']").remove();
        }
    });
    
    $("#to").on("change", function() {
        if($("#to").val() == "") {
            $("#download-error-list").append("<li id='to-error'>Please select an end date for report.</li>");
        } else {
            $("li[id='to-error']").remove();
        }
    });
    
});

$( function() {
    var dateFormat = "dd/mm/yy",
    from = $( "#from" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        dateFormat: 'dd/mm/yy'
    }).datepicker("setDate", new Date()).on( "change", function() {
        to.datepicker( "option", "minDate", getDate( this ) );
    }),
        
        
    to = $( "#to" ).datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        numberOfMonths: 3,
        dateFormat: 'dd/mm/yy'
    }).datepicker("setDate", new Date()).on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
    });
 
    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    }
});

$(function() {
    $(document).on("click", "[data-toggle]", function() {
        $("#lines-"+$(this).data("toggle")).toggle();
        $(this).children("i.glyphicon").toggleClass("glyphicon-circle-arrow-right glyphicon-circle-arrow-down");
    });
});

</script>
@endsection