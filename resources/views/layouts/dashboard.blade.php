<div class="table-responsive">
<table id="invoice-table" class="table-striped table-bordered table-hover" border="3">
    <thead>
        <tr>
            <th id="action-col">Actions</th>
            <th class="clickable" data-column="invoice_number" data-orderby="asc">Invoice<br>Number <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="clickable" data-column="invoice_date" data-orderby="asc">Invoice<br>Date <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="wide_col" data-column="buyers.name" data-orderby="asc">Buyer</i></th>
            <th class="wide_col">Transport</th>
            <th class="clickable" data-column="cgst_percent" data-orderby="asc">CGST % <i class="glyphicon glyphicon-sort pull-right adjust-single-line-header"></i></th>
            <th class="clickable" data-column="cgst_amount" data-orderby="asc">CGST<br>Amount <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="clickable" data-column="sgst_percent" data-orderby="asc">SGST % <i class="glyphicon glyphicon-sort pull-right adjust-single-line-header"></i></th>
            <th class="clickable" data-column="sgst_amount" data-orderby="asc">SGST<br>Amount <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="clickable" data-column="igst_percent" data-orderby="asc">IGST % <i class="glyphicon glyphicon-sort pull-right adjust-single-line-header"></i></th>
            <th class="clickable" data-column="igst_amount" data-orderby="asc">IGST<br>Amount <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="clickable" data-column="invoice_total" data-orderby="asc">Invoice<br>Total <i class="glyphicon glyphicon-sort pull-right"></i></th>
            <th class="clickable" data-column="grand_total" data-orderby="asc">Grand<br>Total <i class="glyphicon glyphicon-sort pull-right"></i></th>
        </tr>
    </thead>
    <tbody>
    @if (!is_null($invoices) || !$invoices->isEmpty())
        @foreach($invoices as $invoice)
            <tr>
                <td>
                    <button type="button" data-toggle="{{$invoice->invoice_number}}" class="btn btn-red btn-circle"><i class="glyphicon glyphicon-circle-arrow-right"></i></button>
                    <button type="button" data-btn-type="print" data-invoice-number="{{$invoice->slug}}" class="btn btn-success btn-circle"><i class="glyphicon glyphicon-print"></i></button>
                    <button type="button" data-btn-type="printview" data-invoice-number="{{$invoice->slug}}" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-eye-open"></i></button>            
                    <button type="button" data-btn-type="editinvoice" data-invoice-number="{{$invoice->slug}}" data-url="{{route('sales', ["invoiceSlug" => $invoice->slug])}}" class="btn btn-circle btn-tarquine"><i class="glyphicon glyphicon-edit"></i></button>
                </td>
                <td>@if ($invoice->invoice_number) {{ $invoice->invoice_number }} @else {{ '-' }} @endif</td>
                
                <?php
					$date = DateTime::createFromFormat('Y-m-d', $invoice->invoice_date);
				?>
                
                <td>@if ($invoice->invoice_date) {{ $date->format('d/m/Y') }} @else {{ '-' }} @endif</td>
                <td>@if ($invoice->buyer->name) {{ $invoice->buyer->name }} @else {{ '-' }} @endif</td>
                <td>@if ($invoice->transport["transport_name"]) {{ $invoice->transport["transport_name"] }} @else {{ '-' }} @endif</td>
                <td>@if ($invoice->cgst_percent) {{ sprintf("%0.2f%%", $invoice->cgst_percent) }} @else {{ '-' }} @endif</td>
                <td class="amount_col">@if ($invoice->cgst_amount) {{ sprintf("%0.2f", $invoice->cgst_amount) }} @else {{ '-' }} @endif</td>
                <td>@if ($invoice->sgst_percent) {{ sprintf("%0.2f%%", $invoice->sgst_percent) }} @else {{ '-' }} @endif</td>
                <td class="amount_col">@if ($invoice->sgst_amount) {{ sprintf("%0.2f", $invoice->sgst_amount) }} @else {{ '-' }} @endif</td>
                <td>@if ($invoice->igst_percent) {{ sprintf("%0.2f%%", $invoice->igst_percent) }} @else {{ '-' }} @endif</td>
                <td class="amount_col">@if ($invoice->igst_amount) {{ sprintf("%0.2f", $invoice->igst_amount) }} @else {{ '-' }} @endif</td>
                <td class="amount_col">@if ($invoice->invoice_total) {{ sprintf("%0.2f", $invoice->invoice_total) }} @else {{ '-' }} @endif</td>
                <td class="amount_col">@if ($invoice->grand_total) {{ sprintf("%0.2f", $invoice->grand_total) }} @else {{ '-' }} @endif</td>
                
            </tr>
            
            <tr id="lines-{{$invoice->invoice_number}}" style="display:none">
               <td colspan="2"><strong>@if ($invoice->invoice_number) {{ 'Lines for invoice #: '.$invoice->invoice_number }} @else {{ '-' }} @endif</strong></td>
               <td colspan="11">
                   <div class="table-responsive">
                   <table class="table-striped table-bordered table-hover" border=3>
                       <thead>
                           <th>Line #</th>
                           <th>Item</th>
                           <th>Quantity</th>
                           <th>Unit</th>
                           <th>Rate</th>
                           <th>Discount%</th>
                           <th>Amount</th>
                       </thead>
                       <tbody>
                           @foreach($invoice->invoice_lines as $key => $value)
                           <tr>
                               <td>{{$key+1}}</td>
                               <td>{{$value->item['description']}}</td>
                               <td>{{$value["quantity"]}}</td>
                               <td>@if(!empty($value->unit['short_name'])) {{$value->unit['short_name']}} @else - @endif</td>
                               <td>{{$value["rate"]}}</td>
                               <td>@if(!empty($value["discount"])) {{ sprintf("%0.2f%%", $value["discount"]) }} @else  -  @endif</td>
                               <td>{{$value["amount"]}}</td>
                           </tr>
                           @endforeach
                       </tbody>
                   </table>
                   </div>
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7">
                <h3>No invoices found. Create some awesome invoices to see them here.</h3>
            </td>
        </tr>
    @endif
    </tbody>
</table>
</div>
@if(!is_null($invoices) || !$invoices->isEmpty())
<div class="row" align="center">
    {{$invoices->links()}}
</div>
@endif
