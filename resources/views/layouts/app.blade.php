<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Billing') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    
    
    
    @yield('pagecss')
    
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet">
    
    <style type="text/css">
        
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Billing') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (Auth::check())
                            <li><a href="{{route('home')}}">dashboard</a></li>
                            <li><a href="{{route('sales')}}">sales invoice</a></li>
                            <li><a class="dropdown-toggle" data-toggle="dropdown" href="#">manage
                                <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="{{ route('buyers') }}">buyers</a></li>
                                  <li><a href="{{ route('items') }}">items</a></li>
                                  <li><a href="{{ route('units') }}">units</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <!--<li><a href="{{ route('register') }}">Register</a></li>-->
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- header for all pages -->
        
        <div id="slider" align="center" class="hidden-xs">
          <ul>
            <li>
                <img src="{{ asset('images/ahvn.png') }}" class="img-responsive" style="margin-top:4em" width="auto"/>
            </li>
            <li>
                <img src="{{ asset('images/mb_logo.png') }}" class="img-responsive"/>
            </li>
          </ul>  
        </div>
        <!-- Spinner for AJAX loading -->
        <div id="processing_image" class="overlay">
    		<div class="processing_wrapper">
    			<div class="processing_img">
    				<div id="preloader"></div>
    				<span class="preloader_span" id="loader_text">Processing.....</span>
    			</div>
    		</div>
    	</div>
        @yield('content')
    </div>

    <!-- Scripts -->
    
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery-ui.js') }}"></script>
    
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

          $(function(){
            setInterval(function () {
                moveRight();
            }, 6000);
          });
          
        	var slideCount = $('#slider ul li').length;
        	var slideWidth = $('#slider ul li').width();
        	var slideHeight = $('#slider ul li').height();
        	var sliderUlWidth = slideCount * slideWidth;
        	
        	$('#slider').css({ width: slideWidth, height: slideHeight });
        	
        	$('#slider ul').css({ width: sliderUlWidth, marginLeft: - slideWidth });
        	
            $('#slider ul li:last-child').prependTo('#slider ul');
        
            function moveLeft() {
                $('#slider ul').animate({
                    left: + slideWidth
                }, 200, function () {
                    $('#slider ul li:last-child').prependTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            };
        
            function moveRight() {
                $('#slider ul').animate({
                    left: - slideWidth
                }, 200, function () {
                    $('#slider ul li:first-child').appendTo('#slider ul');
                    $('#slider ul').css('left', '');
                });
            };
        
            $('a.control_prev').click(function () {
                moveLeft();
            });
        
            $('a.control_next').click(function () {
                moveRight();
            });
        
        });    

    </script>
    
    
    @yield('script')
</body>
</html>