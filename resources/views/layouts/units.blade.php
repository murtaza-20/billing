<div class="row clearfix">
	<div class="col-md-12 table-responsive">
		<table class="table table-bordered table-hover unit_table" id="invoice-table" border="3">
			<thead>
				<tr>
					<th id="action-col">Actions</th>
					<th class="clickable" data-column="name" data-orderby="asc">Name <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="short_name" data-orderby="asc">Short Name <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="sort_order" data-orderby="asc">Sort Order <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="status" data-orderby="asc">Status <i class="glyphicon glyphicon-sort pull-right"></i></th>
				</tr>
			</thead>
			<tbody>
				@if (!is_null($units) || !$units->isEmpty())
					@foreach($units as $key => $unit) 
						<tr id="{{$key}}">
							<td>
								<button type="button" data-btn-type="delete" data-buyer="{{$unit->id}}" class="btn btn-red btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></button>
								<button type="button" data-btn-type="update" data-buyer="{{$unit->id}}" data-row-id="{{$key}}" class="btn btn-warning btn-circle"><i class="glyphicon glyphicon-edit"></i></button>
								<button type="button" data-btn-type="activate" data-buyer="{{$unit->id}}" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-remove"></i></button>
							</td>
							<td><input type="text" name="name" id="" placeholder="Unit Name" class="form-control" value="@if ($unit->name) {{ $unit->name }} @endif" readonly />
							    <input type="hidden" name="unit_id" value="{{$unit->id}}"/></td>
							<td><input type="text" name="short_name" id="" placeholder="Short Name" class="form-control" value="@if ($unit->short_name) {{ $unit->short_name }} @endif" readonly /></td>
							<td><input type="text" name="sort_order" id="" placeholder="Sort Order" class="form-control" value="@if ($unit->sort_order) {{ $unit->sort_order }} @endif" readonly /></td>
							<td>
								<select name="status" id="" class="form-control" disabled="disabled">
									<option value='Activated' @if($unit->status == 'Activated') selected @endif)> Activated</option>
									<option value='De-Activated' @if($unit->status == 'De-Activated') selected @endif)>De-Activated</option>
								</select>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@if(!is_null($units) || !$units->isEmpty())
<div class="row" align="center">
    {{$units->links()}}
</div>
@endif
