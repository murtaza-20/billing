<div class="row clearfix">
	<div class="col-md-12 table-responsive">
		<table class="table table-bordered table-hover item_table" id="invoice-table" border="3">
			<thead>
				<tr>
					<th id="action-col">Actions</th>
					<th class="clickable" data-column="description" data-orderby="asc">Name/Description <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="hsn_code" data-orderby="asc">HSN Code <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="status" data-orderby="asc">Status <i class="glyphicon glyphicon-sort pull-right"></i></th>
				</tr>
			</thead>
			<tbody>
				@if (!is_null($items) || !$items->isEmpty())
					@foreach($items as $key => $item) 
						<tr id="{{$key}}">
							<td>
								<button type="button" data-btn-type="delete" data-buyer="{{$item->id}}" class="btn btn-red btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></button>
								<button type="button" data-btn-type="update" data-buyer="{{$item->id}}" data-row-id="{{$key}}" class="btn btn-warning btn-circle"><i class="glyphicon glyphicon-edit"></i></button>
								<button type="button" data-btn-type="activate" data-buyer="{{$item->id}}" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-remove"></i></button>
							</td>
							<td><input type="text" name="description" id="" placeholder="Item Name" class="form-control" value="@if ($item->description) {{ $item->description }} @endif" readonly />
							    <input type="hidden" name="item_id" value="{{$item->id}}"/></td>
							<td><input type="text" name="hsn_code" id="" placeholder="HSN Code" class="form-control" value="@if ($item->hsn_code) {{ $item->hsn_code }} @endif" readonly /></td>
							<td>
								<select name="status" id="" class="form-control" disabled="disabled">
									<option value='Activated' @if($item->status == 'Activated') selected @endif)> Activated</option>
									<option value='De-Activated' @if($item->status == 'De-Activated') selected @endif)>De-Activated</option>
								</select>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@if(!is_null($items) || !$items->isEmpty())
<div class="row" align="center">
    {{$items->links()}}
</div>
@endif
