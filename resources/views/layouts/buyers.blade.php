<div class="row clearfix">
	<div class="col-md-12 table-responsive">
		<table class="table table-bordered table-hover buyer_table" id="invoice-table" border="3">
			<thead>
				<tr>
					<th id="action-col">Actions</th>
					<th class="clickable" data-column="name" data-orderby="asc">Name <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="address" data-orderby="asc">Address <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="address_2" data-orderby="asc">Address 2 <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="mobile" data-orderby="asc">Mobile <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="email_address" data-orderby="asc">Email <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="gstn" data-orderby="asc">GSTN <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="city" data-orderby="asc">City <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="state" data-orderby="asc">State <i class="glyphicon glyphicon-sort pull-right"></i></th>
					<th class="clickable" data-column="status" data-orderby="asc">Status <i class="glyphicon glyphicon-sort pull-right"></i></th>
				</tr>
			</thead>
			<tbody>
				@if (!is_null($buyers) || !$buyers->isEmpty())
					@foreach($buyers as $key => $buyer) 
						<tr id="{{$key}}">
							<td>
								<button type="button" data-btn-type="delete" data-buyer="{{$buyer->id}}" class="btn btn-red btn-circle"><i class="glyphicon glyphicon-minus-sign"></i></button>
								<button type="button" data-btn-type="update" data-buyer="{{$buyer->id}}" data-row-id="{{$key}}" class="btn btn-warning btn-circle"><i class="glyphicon glyphicon-edit"></i></button>
								<button type="button" data-btn-type="activate" data-buyer="{{$buyer->id}}" class="btn btn-info btn-circle"><i class="glyphicon glyphicon-remove"></i></button>
							</td>
							<td><input type="text" name="name" id="" placeholder="Buyer Name" class="form-control" value="@if ($buyer->name) {{ $buyer->name }} @endif" readonly />
								<input type="hidden" name="buyer_id" value="{{$buyer->id}}"/></td>
							<td><input type="text" name="address" id="" placeholder="Address" class="form-control" value="@if ($buyer->address) {{ $buyer->address }} @endif" readonly /></td>
							<td><input type="text" name="address_2" id="" placeholder="Address 2" class="form-control" value="@if ($buyer->address_2) {{ $buyer->address_2 }} @endif" readonly /></td>
							<td><input type="text" name="mobile" id="" placeholder="Mobile" class="form-control" value="@if ($buyer->mobile) {{ $buyer->mobile }} @endif" readonly /></td>
							<td><input type="text" name="email_address" id="" placeholder="Email" class="form-control" value="@if ($buyer->email_address) {{ $buyer->email_address }} @endif" readonly /></td>
							<td><input type="text" name="gstn" id="" placeholder="GSTN" class="form-control" value="@if ($buyer->gstn) {{ $buyer->gstn }} @endif" readonly /></td>
							<td><input type="text" name="city" id="" placeholder="City" class="form-control" value="@if ($buyer->city) {{ $buyer->city }} @endif" readonly /></td>
							<td><input type="text" name="state" id="" placeholder="State" class="form-control" value="@if ($buyer->state) {{ $buyer->state }} @endif" readonly /></td>
							<td>
								<select name="status" id="" class="form-control" disabled="disabled">
									<option value='Activated' @if($buyer->status == 'Activated') selected @endif)> Activated</option>
									<option value='De-Activated' @if($buyer->status == 'De-Activated') selected @endif)>De-Activated</option>
								</select>
							</td>
						</tr>
					@endforeach
				@endif
			</tbody>
		</table>
	</div>
</div>
@if(!is_null($buyers) || !$buyers->isEmpty())
<div class="row" align="center">
    {{$buyers->links()}}
</div>
@endif
