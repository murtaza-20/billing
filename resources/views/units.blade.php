@extends('layouts.app')

@section('pagecss')

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Unit Management</div>
                <div class="panel-body">
                    <div class="col-md-12" style="border: 1px solid black;border-radius: 5px;padding: 10px; padding-bottom: 0px;margin-top:5px">
                        <p>
                            Add units individually with "Add Individual Unit" utility.
                            <div align="center">
                                <div class="form-group">
                                    <label class="col-lg-3" for="description">Unit Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="name" id="name" placeholder="Unit Name" class="form-control" value="{{ old('name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="short_name">Short Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="short_name" id="short_name" placeholder="Short Name" class="form-control" value="{{ old('short_name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="sort_order">Sort Order</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="sort_order" id="sort_order" placeholder="Sort Order" class="form-control" value="{{ old('sort_order') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="status">Status</label>
                                    <div class="col-lg-8">
                                        <select name="status" id="status" class="form-control">
                                            <option value="Activated">Activated</option>
                                            <option value="De-Activated">De-Activated</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="button" name="add-unit" class="btn btn-primary btn-sm" style="margin-top:10px;"><i class="glyphicon glyphicon-plus"></i> Add Individual Unit</button>
                            </div>
                        </p>
                    </div>
                </div>    
            </div>    
        </div>            
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">All Items</div>
                    <div class="panel-body">
                        <div class="row table_header" style="margin-top: 3px;">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="no_of_record">No. of Records: </label>
                                        <select name="no_of_record" id="no_of_record" class="form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12" style="margin-top: 50px">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="search">Search terms: </label>
                                        <input type="text" name="search" id="search" class="form-control input-sm" placeholder="Search" style="margin-top: 0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="unit-view">
                            {!! $unit_dashboard !!}
                        </div>
                    </div>
                    <div id="dialog" title=""></div>
                </div>
            </div>
        </div>
    </div>
</div>
                

@endsection

@section('script')
<script type="text/javascript">
    function showProcessing(processingText="Please wait...") {
        $("#loader_text").text(processingText);
        $("#processing_image.overlay").css({"visibility" :"visible", "opacity" :"1"});
    }
    
    function stopProcessing() {
        $("#processing_image.overlay").css({"visibility" :"hidden", "opacity" :"0"});
    }
</script>

<script type="text/javascript">

function filter(column, orderby) {
    showProcessing("Ordering, wait...");
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('sort_unit') }}",
        data: {
            "column"      : column,
            "orderby"     : orderby,
            "no_of_record": $("#no_of_record").val(),
            "search"      : $("#search").val()
        },
        method: "POST",
        dataType: "json",
        complete: function(response) {
            stopProcessing();
        },
        success: function(response) {
            $(".unit-view").empty();
            $(".unit-view").html(response.html);
            
            if(orderby == "desc") {
                $("th[data-column='"+column+"']").data("orderby", "asc");
            } else {
                $("th[data-column='"+column+"']").data("orderby", "desc");
            }
        },
        failure: function(response) {}
    });
}


$(function(){
   $(document).on("click", ".clickable", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("keyup", "#search", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("change", "#no_of_record", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
});
</script>

<script type="text/javascript">
    $(function(){
       $("[name='upload-template']").on("click", function(){
           
       });
    });
</script>

<script type="text/javascript">
    /*global $*/
    $(function(){
       $(document).on("click", "[data-btn-type]", function(){
          if ($(this).data("btn-type") == "update" && $(this).children("i").hasClass("glyphicon-edit")) {
            $("#"+$(this).data("row-id")+" td").each(function(key, value){
                $(value).find("input").removeAttr("readonly");
                $(value).find("select").removeAttr("disabled");
                $(value).find("input").css({"background-color":"#265634", "color":"#FFFFFF", "font-weight":"bold"});
            });
            
            $(this).children("i").toggleClass ("glyphicon-edit glyphicon-floppy-save");
            $(this).css({"background-color":"#265634"});
          }
          
          else if ($(this).data("btn-type") == "update" && $(this).children("i").hasClass("glyphicon-floppy-save")) {
              
            var that = $(this);
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "{{route('save_single_record')}}",
              data: {
                  "form"        : "unit",
                  "type"        : "update-unit",
                  "id"          : $(that).parent().parent().find("input[name='unit_id']").val(),
                  "name"        : $(this).parent().parent().find("input[name='name']").val(),
                  "short_name"  : $(this).parent().parent().find("input[name='short_name']").val(),
                  "sort_order"  : $(this).parent().parent().find("input[name='sort_order']").val(),
                  "status"      : $(this).parent().parent().find("select[name='status'] option:selected").val()
              },
              dataType: "json",
              method: "post",
              beforeSend: function() {
                showProcessing("Saving Unit...");  
              },
              complete: function(response){
                stopProcessing();
              },
              success: function(response){
                if (response.type == "success") {
                    
                    $(".ui-dialog-title").text("success");
                    $("#dialog").html(response.msg);
                    
                    $("#dialog").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                $(".ui-dialog-title").html("");
                                $("#dialog").html("");
                            }
                        }
                    });
                    
                    $(that).parent().parent().each(function(key, value){
                        $(value).find("input").attr("readonly", "readonly");
                        $(value).find("select").attr("disabled", "disabled");
                        $(value).find("input").css({"background-color":"#eee", "color":"#555", "font-weight":"normal"});
                        
                        $(value).find("button[data-btn-type='update']").css({"background-color": "#cbb956"});
                        $(value).find("button[data-btn-type='update']").children("i").toggleClass("glyphicon-floppy-save glyphicon-edit");
                    });
                    
                }  
              },
              failure: function(response){}
            });
          }
          
          else if ($(this).data("btn-type") == "delete") {
            
            var that = $(this);
            $(".ui-dialog-title").html("Delete Unit?");
            $("#dialog").html("Are you sure about deleting this unit?");
            
            $("#dialog").dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        $(".ui-dialog-title").html("");
                        $("#dialog").html("");
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{route('delete_single_record')}}",
                            data: {
                                form : "unit",
                                id   : $(that).parent().parent().find("input[name='unit_id']").val()
                            },
                            dataType : "json",
                            method   : "post",
                            beforeSend : function() {
                                showProcessing("Deleting Unit...");
                            }, 
                            complete : function (response) {
                                stopProcessing();
                            },
                            success: function(response) {
                                if (response.type == "success") {
                                    $(".ui-dialog-title").html(response.type);
                                    $("#dialog").html(response.msg);
                                    $("#dialog").dialog({
                                        modal: true,
                                        buttons: {
                                            Ok: function() {
                                                $(this).dialog("close");
                                                $(".ui-dialog-title").html("");
                                                $("#dialog").html("");
                                            }
                                        }
                                    });
                                    
                                    $(that).parent().parent().remove();
                                }
                            }
                        })
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $(".ui-dialog-title").html("");
                        $("#dialog").html("");
                    }
                }
            });
          }
       });
       
       $(document).on("keydown", function(e){
            
            if (e.keyCode == 27) {
                e.preventDefault();
                $(".glyphicon-floppy-save").parent().parent().parent().find("td").each(function(key, value){
                    $(value).find("input").attr("readonly", "readonly");
                    $(value).find("select").attr("disabled", "disabled");
                    $(value).find("input").css({"background-color":"#eee", "color":"#555", "font-weight":"normal"});
                    
                    $(value).find("button[data-btn-type='update']").css({"background-color": "#cbb956"});
                    $(value).find("button[data-btn-type='update']").children("i").toggleClass("glyphicon-floppy-save glyphicon-edit");
                    
                });
                
            } 
       });
    });
</script>

<script type="text/javascript">
    $(function(){
    
        $("button[name='add-unit']").on("click", function(){
           $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "{{route('save_single_record')}}",
              data: {
                  "form"         : "unit",
                  "type"         : "add-unit",
                  "name"         : $("#name").val(),
                  "short_name"   : $("#short_name").val(),
                  "sort_order"   : $("#sort_order").val(),
                  "status"       : $("#status option:selected").val()
              },
              dataType: "json",
              method: "post",
              beforeSend: function() {
                showProcessing("Saving Unit...");  
              },
              complete: function(response){
                stopProcessing();
              },
              success: function(response){
                if (response.type == "success") {
                    $("#dialog").attr("title", response.type);
                    $("#dialog").html(response.msg);
                    $("#dialog").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                $(".ui-dialog-title").html("");
                                $("#dialog").html("");
                            }
                        }
                    });
                    
                    $("#name").val('');
                    $("#short_name").val('');
                    $("#sort_order").val('');
                    $('#status').prop('selectedIndex',0);
                    
                    $(".unit-view").html();
                    $(".unit-view").html(response.html); 
                }  
              },
              failure: function(response){}
            }); 
        });
    });
</script>
@endsection