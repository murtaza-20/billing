@extends('layouts.app')

@section('pagecss')

@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Buyer Management</div>
                <div class="panel-body">
                    <div class="col-md-12" style="border: 1px solid black;border-radius: 5px;padding: 10px; padding-bottom: 5px;">
                        <p>
                            You can upload multiple buyers with "Bulk Upload Buyers" option.
                            <ul>
                                <li><a href="{{route('download_bulk_import_buyer_template')}}" target="_blank">Click here to download the template.</a></li>
                                <li>Fill in your buyer data.</li>
                                <li>Upload the file in "Choose File" and click "Bulk Upload Buyers".</li>
                                <li>You are good to go.</li>
                            </ul>
                        </p>
                        <div align="center">
                            @if (session()->has('msg') && session()->get('msg') == 'excel_upload_exception')
                            <div class="panel panel-danger">
                                <div class="panel-body">
                                    <div class="col-md-12" style="color: red">
                                        You seem to have uploaded invalid excel format. Please download the template from <a href="{{route('download_bulk_import_buyer_template')}}" target="_blank">this</a> link, fill it up and upload again.
                                    </div>
                                </div>
                            </div>
                            <?php session()->forget('msg'); ?>
                            @endif
                            
                            <form action="{{route('bulk_import')}}" method="post" enctype="multipart/form-data">
                                <input type="file" name="buyer_file" style="margin-bottom: 5px;"/>
                                <input type="hidden" name="form" value="buyer">
                                <input type="hidden" name="type" value="bulk-upload"/>
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <button type="submit" name="upload-template" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-upload"></i> Bulk Upload Buyers</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-12" style="border: 1px solid black;border-radius: 5px;padding: 10px; padding-bottom: 0px;margin-top:5px">
                        <p>
                            Alternatively add buyers individually with "Add Individual Buyer" utility.
                            <div align="center">
                                <div class="form-group">
                                    <label class="col-lg-3" for="name">Buyer Name</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="address">Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="address" id="address" placeholder="Address" class="form-control" value="{{ old('address') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="address_2">Address 2</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="address_2" id="address_2" placeholder="Address 2" class="form-control" value="{{ old('address_2') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="mobile">Mobile</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="mobile" id="mobile" placeholder="Mobile" class="form-control" value="{{ old('mobile') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="email_address">Email Address</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="email_address" id="email_address" placeholder="Email Address" class="form-control" value="{{ old('email_address') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="gstn">GSTN</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="gstn" id="gstn" placeholder="GSTN" class="form-control" value="{{ old('gstn') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="city">City</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="city" id="city" placeholder="City" class="form-control" value="{{ old('city') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="state">State</label>
                                    <div class="col-lg-8">
                                        <input type="text" name="state" id="state" placeholder="State" class="form-control" value="{{ old('state') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3" for="status">Status</label>
                                    <div class="col-lg-8">
                                        <select name="status" id="status" class="form-control">
                                            <option value="Activated">Activated</option>
                                            <option value="De-Activated">De-Activated</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="button" name="add-buyer" class="btn btn-primary btn-sm" style="margin-top:10px;"><i class="glyphicon glyphicon-plus"></i> Add Individual Buyer</button>
                            </div>
                        </p>
                    </div>
                </div>    
            </div>    
        </div>            
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">All Buyers</div>
                    <div class="panel-body">
                        <div class="row table_header" style="margin-top: 3px;">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="no_of_record">No. of Records: </label>
                                        <select name="no_of_record" id="no_of_record" class="form-control">
                                            <option value="10">10</option>
                                            <option value="25">25</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                            <option value="500">500</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12" style="margin-top: 50px">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="search">Search terms: </label>
                                        <input type="text" name="search" id="search" class="form-control input-sm" placeholder="Search" style="margin-top: 0"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="buyer-view">
                            {!! $buyers_dashboard !!}
                        </div>
                    </div>
                    <div id="dialog" title=""></div>
                </div>
            </div>
        </div>
    </div>
</div>
                

@endsection

@section('script')
<script type="text/javascript">
    function showProcessing(processingText="Please wait...") {
        $("#loader_text").text(processingText);
        $("#processing_image.overlay").css({"visibility" :"visible", "opacity" :"1"});
    }
    
    function stopProcessing() {
        $("#processing_image.overlay").css({"visibility" :"hidden", "opacity" :"0"});
    }
</script>

<script type="text/javascript">

function filter(column, orderby) {
    showProcessing("Ordering, wait...");
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('sort_buyer') }}",
        data: {
            "column":       column,
            "orderby":      orderby,
            "no_of_record": $("#no_of_record").val(),
            "search":       $("#search").val()
        },
        method: "POST",
        dataType: "json",
        complete: function(response) {
            stopProcessing();
        },
        success: function(response) {
            $(".buyer-view").empty();
            $(".buyer-view").html(response.html);
            
            if(orderby == "desc") {
                $("th[data-column='"+column+"']").data("orderby", "asc");
            } else {
                $("th[data-column='"+column+"']").data("orderby", "desc");
            }
        },
        failure: function(response) {}
    });
}


$(function(){
   $(document).on("click", ".clickable", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("keyup", "#search", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
    
    $(document).on("change", "#no_of_record", function(){
        var column    = $(this).data("column");
        var orderby   = $(this).data("orderby");
        filter(column, orderby);
    });
});
</script>

<script type="text/javascript">
    $(function(){
       $("[name='upload-template']").on("click", function(){
           
       });
    });
</script>

<script type="text/javascript">
    /*global $*/
    $(function(){
       $(document).on("click", "[data-btn-type]", function(){
          if ($(this).data("btn-type") == "update" && $(this).children("i").hasClass("glyphicon-edit")) {
            $("#"+$(this).data("row-id")+" td").each(function(key, value){
                $(value).find("input").removeAttr("readonly");
                $(value).find("select").removeAttr("disabled");
                $(value).find("input").css({"background-color":"#265634", "color":"#FFFFFF", "font-weight":"bold"});
            });
            
            $(this).children("i").toggleClass ("glyphicon-edit glyphicon-floppy-save");
            $(this).css({"background-color":"#265634"});
          }
          
          else if ($(this).data("btn-type") == "update" && $(this).children("i").hasClass("glyphicon-floppy-save")) {
              
            var that = $(this);
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "{{route('save_single_record')}}",
              data: {
                  "form"     : "buyer",
                  "type"     : "update-buyer",
                  "id"       : $(this).parent().parent().find("input[name='buyer_id']").val(),
                  "name"     : $(this).parent().parent().find("input[name='name']").val(),
                  "address"  : $(this).parent().parent().find("input[name='address']").val(),
                  "address_2": $(this).parent().parent().find("input[name='address_2']").val(),
                  "mobile"   : $(this).parent().parent().find("input[name='mobile']").val(),
                  "email"    : $(this).parent().parent().find("input[name='email_address']").val(),
                  "gstn"     : $(this).parent().parent().find("input[name='gstn']").val(),
                  "city"     : $(this).parent().parent().find("input[name='city']").val(),
                  "state"    : $(this).parent().parent().find("input[name='state']").val(),
                  "status"   : $(this).parent().parent().find("select[name='status'] option:selected").val()
              },
              dataType: "json",
              method: "post",
              beforeSend: function() {
                showProcessing("Saving Buyer...");  
              },
              complete: function(response){
                stopProcessing();
              },
              success: function(response){
                if (response.type == "success") {
                    
                    $(".ui-dialog-title").text("success");
                    $("#dialog").html(response.msg);
                    
                    $("#dialog").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                $(".ui-dialog-title").html("");
                                $("#dialog").html("");
                            }
                        }
                    });
                    
                    $(that).parent().parent().each(function(key, value){
                        $(value).find("input").attr("readonly", "readonly");
                        $(value).find("select").attr("disabled", "disabled");
                        $(value).find("input").css({"background-color":"#eee", "color":"#555", "font-weight":"normal"});
                        
                        $(value).find("button[data-btn-type='update']").css({"background-color": "#cbb956"});
                        $(value).find("button[data-btn-type='update']").children("i").toggleClass("glyphicon-floppy-save glyphicon-edit");
                    });
                    
                }  
              },
              failure: function(response){}
            });
          }
          
          else if ($(this).data("btn-type") == "delete") {
            
            var that = $(this);
            $(".ui-dialog-title").html("Delete Buyer?");
            $("#dialog").html("Are you sure about deleting this buyer?");
            
            $("#dialog").dialog({
                modal: true,
                buttons: {
                    Ok: function() {
                        $(this).dialog("close");
                        $(".ui-dialog-title").html("");
                        $("#dialog").html("");
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: "{{route('delete_single_record')}}",
                            data: {
                                form : "buyer",
                                id   : $(that).parent().parent().find("input[name='buyer_id']").val()
                            },
                            dataType : "json",
                            method   : "post",
                            beforeSend : function() {
                                showProcessing("Deleting Buyer...");
                            }, 
                            complete : function (response) {
                                stopProcessing();
                            },
                            success: function(response) {
                                if (response.type == "success") {
                                    $(".ui-dialog-title").html(response.type);
                                    $("#dialog").html(response.msg);
                                    $("#dialog").dialog({
                                        modal: true,
                                        buttons: {
                                            Ok: function() {
                                                $(this).dialog("close");
                                                $(".ui-dialog-title").html("");
                                                $("#dialog").html("");
                                            }
                                        }
                                    });
                                    
                                    $(that).parent().parent().remove();
                                }
                            }
                        })
                    },
                    Cancel: function () {
                        $(this).dialog("close");
                        $(".ui-dialog-title").html("");
                        $("#dialog").html("");
                    }
                }
            });
          }
       });
       
       $(document).on("keydown", function(e){
            
            if (e.keyCode == 27) {
                e.preventDefault();
                $(".glyphicon-floppy-save").parent().parent().parent().find("td").each(function(key, value){
                    $(value).find("input").attr("readonly", "readonly");
                    $(value).find("select").attr("disabled", "disabled");
                    $(value).find("input").css({"background-color":"#eee", "color":"#555", "font-weight":"normal"});
                    
                    $(value).find("button[data-btn-type='update']").css({"background-color": "#cbb956"});
                    $(value).find("button[data-btn-type='update']").children("i").toggleClass("glyphicon-floppy-save glyphicon-edit");
                    
                });
                
            } 
       });
    });
</script>

<script type="text/javascript">
    $(function(){
    
        $("button[name='add-buyer']").on("click", function(){
           $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url: "{{route('save_single_record')}}",
              data: {
                  "form"     : "buyer",
                  "type"     : "add-buyer",
                  "name"     : $("#name").val(),
                  "address"  : $("#address").val(),
                  "address_2": $("#address_2").val(),
                  "mobile"   : $("#mobile").val(),
                  "email"    : $("#email_address").val(),
                  "gstn"     : $("#gstn").val(),
                  "city"     : $("#city").val(),
                  "state"    : $("#state").val(),
                  "status"   : $("#status option:selected").val()
              },
              dataType: "json",
              method: "post",
              beforeSend: function() {
                showProcessing("Saving Buyer...");  
              },
              complete: function(response){
                stopProcessing();
              },
              success: function(response){
                if (response.type == "success") {
                    $("#dialog").attr("title", response.type);
                    $("#dialog").html(response.msg);
                    $("#dialog").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                                $(".ui-dialog-title").html("");
                                $("#dialog").html("");
                            }
                        }
                    });
                    
                    $("#name").val('');
                    $("#address").val('');
                    $("#address_2").val('');
                    $("#mobile").val('');
                    $("#email_address").val('');
                    $("#gstn").val('');
                    $("#city").val('');
                    $("#state").val('');
                    $('#status').prop('selectedIndex',0);
                    
                    $(".buyer-view").html();
                    $(".buyer-view").html(response.html);
                        
                }  
              },
              failure: function(response){}
            }); 
        });
    });
</script>
@endsection