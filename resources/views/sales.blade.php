@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        
        <form method="POST" action="{{route('save_invoice')}}">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <input type="hidden" name="print" value="no">
        <input type="hidden" name="preview" value="no">
        @isset($invoiceHeader)
            <input type="hidden" name="invoiceSlug" value="{{$invoiceHeader->slug}}">
        @endisset
        <div class="col-md-12">
            
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <ul>
                        @foreach ($errors->all() as $error) 
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (Session::has('message') && Session::has('type'))
                <div class="alert alert-{{Session::get('type')}} alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    {!! Session::get('message') !!}
                </div>
            @endif
            
            <div class="panel panel-default">
                <div class="panel-heading">Invoice Header</div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-lg-3" for="invoice_number">Invoice Number</label>
                        <div class="col-lg-8">
                            @if(isset($invoiceHeader))
                                <input type="text" name="invoice_number" id="invoice_number" readonly placeholder="Invoice Number" class="form-control" value="{{$invoiceHeader->invoice_number}}">
                            @else
                                <input type="text" name="invoice_number" id="invoice_number" readonly placeholder="Invoice Number" class="form-control" value="{{$next_invoice_number}}">
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="invoice_date">Invoice Date</label>
                        <div class="col-lg-8">
                            <input type="text" name="invoice_date" id="invoice_date" placeholder="Invoice Date" class="form-control" @if (isset($invoiceHeader)) rel="{{\Carbon\Carbon::createFromFormat("Y-m-d", $invoiceHeader->invoice_date)->format("d/m/Y")}}" @else rel="{{\Carbon\Carbon::now()->format("d/m/Y")}}" @endif>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="transport">Transport</label>
                        <div class="col-lg-8">
                            <select name="transport" id="transport" style="width: 100%">
                                    <option>Select Transport</option>
                                    @if (Session::has('transport') && !is_null(Session::get('transport')) && !empty(Session::get('transport')) && !is_numeric(Session::get('transport'))) 
                                        <option selected value="{{Session::get('transport')}}">{{Session::get('transport')}}</option>
                                    @endif
                                    @foreach ($transports as $transport)
                                        @if (isset($invoiceHeader) && $transport->id == $invoiceHeader->transport_id)
                                            <option selected value="{{$transport->id}}">{{$transport->transport_name}}</option>
                                        @elseif (old('transport') == $transport->id)
                                            <option selected value="{{$transport->id}}">{{$transport->transport_name}}</option>
                                        @else
                                            <option value="{{$transport->id}}">{{$transport->transport_name}}</option>
                                        @endif
                                    @endforeach
                            </select>
                            <!--<input type="text" name="transport"  id="transport" placeholder="Transport" class="form-control" value="{{old('transport')}}">-->
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="challan_number">Challan</label>
                        <div class="col-lg-8">
                            @if (isset($invoiceHeader))
                                <input type="text" name="challan_number"  id="challan_number" placeholder="Challan" class="form-control" value="{{$invoiceHeader->challan_number}}">
                            @else
                                <input type="text" name="challan_number"  id="challan_number" placeholder="Challan" class="form-control" value="{{old('challan_number')}}">
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Buyer Details</div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-lg-3" for="buyer_id">Buyer</label>
                        <div class="col-lg-8">
                            <select class="buyer_list" class="form-control"  id="buyer_id" name="buyer_id" style="width: 100%">
                                <option value>Select Buyer</option>
                                <?php
                                    $buyer = Session::get('buyer');
                                ?>
                                @if (Session::has('buyer') && !is_null(Session::get('buyer')) && !empty($buyer) && !is_numeric($buyer)) 
                                    <option value="{{$buyer}}" selected="selected">{{$buyer}}</option>
                                @endif
                                @foreach ($buyers as $buyer)
                                    @if (isset($invoiceHeader) && $buyer->id == $invoiceHeader->buyer_id)
                                        <option value="{{$buyer->id}}" selected="selected" data-address="{{$buyer->address}}" data-address_2="{{$buyer->address_2}}" data-mobile="{{$buyer->mobile}}" data-gstn="{{$buyer->gstn}}" data-state="{{$buyer->state}}">{{$buyer->name}}</option>
                                        @php $selectedBuyer = $buyer; @endphp
                                    @elseif (old('buyer_id') == $buyer->id)
                                        <option value="{{$buyer->id}}" selected="selected" data-address="{{$buyer->address}}" data-address_2="{{$buyer->address_2}}" data-mobile="{{$buyer->mobile}}" data-gstn="{{$buyer->gstn}}" data-state="{{$buyer->state}}">{{$buyer->name}}</option>
                                    @else
                                        <option value="{{$buyer->id}}" data-address="{{$buyer->address}}" data-address_2="{{$buyer->address_2}}" data-mobile="{{$buyer->mobile}}" data-gstn="{{$buyer->gstn}}" data-state="{{$buyer->state}}">{{$buyer->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="gstn">GSTN</label>
                        <div class="col-lg-8">
                            @if (isset($selectedBuyer))
                                <input type="text" name="gstn" id="gstn" placeholder="GSTN" class="form-control" value="{{$buyer->gstn}}" readonly="readonly">
                            @else
                                <input type="text" name="gstn" id="gstn" placeholder="GSTN" @if (Session::has('buyer') && is_numeric(Session::get('buyer')))  readonly  @endif class="form-control" value="{{ old('gstn') }}">
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="address">Address Line 1</label>
                        <div class="col-lg-8">
                            @if (isset($selectedBuyer))
                                <input type="text" name="address" id="address" placeholder="Address" class="form-control" value="{{ $buyer->address }}" readonly="readonly">
                            @else
                                <input type="text" name="address" id="address" placeholder="Address" @if (Session::has('buyer') && is_numeric(Session::get('buyer')))  readonly  @endif class="form-control" value="{{ old('address') }}">
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3" for="address_2">Address Line 2</label>
                        <div class="col-lg-8">
                            @if (isset($selectedBuyer))
                                <input type="text" name="address_2" id="address_2" placeholder="Address Line 2" class="form-control" value="{{$buyer->address_2}}" readonly="readonly">
                            @else
                                <input type="text" name="address_2" id="address_2" placeholder="Address Line 2" @if (Session::has('buyer') && is_numeric(Session::get('buyer')))  readonly  @endif class="form-control" value="{{ old('address_2') }}">
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3" for="mobile">Mobile No.</label>
                        <div class="col-lg-8">
                            @if (isset($selectedBuyer))
                                <input type="text" name="mobile" id="mobile" placeholder="mobile" class="form-control" value="{{ $buyer->mobile }}" readonly="readonly">
                            @else
                                <input type="text" name="mobile" id="mobile" placeholder="mobile" @if (Session::has('buyer') && is_numeric(Session::get('buyer')))  readonly  @endif class="form-control" value="{{ old('mobile') }}">
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="state">State</label>
                        <div class="col-lg-8">
                            @if (isset($selectedBuyer))
                                <input type="text" name="state" id="state" placeholder="State" class="form-control" value="{{ $buyer->state }}" readonly="readonly">
                            @else
                                <input type="text" name="state" id="state" placeholder="State" @if (Session::has('buyer') && is_numeric(Session::get('buyer')))  readonly  @endif class="form-control" value="{{ old('state') }}">
                            @endif
                        </div>
                    </div>
                    <?php Session::forget('buyer'); ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Invoice Lines</div>
                <div class="panel-body">
                    <div class="row clearfix">
                        <div class="col-md-12 table-responsive">
                            <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-center" style="padding-left: 90px;padding-right: 90px;padding-bottom: 17px;">Description</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;">HSN Code</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;padding-bottom: 17px;">Qty</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;padding-bottom: 17px;">Unit</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;padding-bottom: 17px;">Rate</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;">LESS Discount(%)</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;padding-bottom: 17px;">Amount</th>
                                        <th class="text-center" style="padding-left: 37px;padding-right: 37px;padding-bottom: 17px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (Session::has("invoice_lines_count"))
                                        @for($i = 0; $i < (int)Session::get("invoice_lines_count"); $i++) 
                                            <tr id="{{'addr'.$i}}">
                                                <td>{{$i+1}}</td>
                                                <td>
                                                    <select name='description[]' id="{{'description'.$i}}" class='form-control' style='width: 100%'>
                                                        <option value>Select Item</option>
                                                        
                                                        @if (Session::has('description.'.$i) && !is_null(Session::get('description.'.$i)) && !empty(Session::get('description.'.$i)) && !is_numeric(Session::get('description.'.$i)) ) 
                                                            <option value="{{Session::get('description.'.$i)}}" selected="selected">{{Session::get('description.'.$i)}}</option>
                                                        @endif
                                                        
                                                        @foreach ($items as $item)
                                                            @if (Session::has('description.'.$i) && Session::get('description.'.$i) == $item->id)
                                                                <option value="{{$item->id}}" data-hsn="{{$item->hsn_code}}" selected="selected">{{$item->description}}</option>
                                                            @else
                                                                <option value="{{$item->id}}" data-hsn="{{$item->hsn_code}}">{{$item->description}}</option>
                                                            @endif
                                                        @endforeach
                                                        
                                                        <?php Session::forget('description.'.$i); ?>
                                                    </select>
                                                </td>
                                                <td><input type="text" name='hsn_code[]' id="{{'hsn_code'.$i}}" placeholder='HSN Code' class="form-control" value="{{old('hsn_code.'.$i)}}" readonly /></td>
                                                <td><input type="text" name='qty[]' id="{{'qty'.$i}}" placeholder='Qty' value="{{old('qty.'.$i)}}" class="form-control"/></td>
                                                <td>
                                                    <select name='unit[]' id="{{'unit'.$i}}" class="form-control unit">
                                                        @foreach ($units as $unit)
                                                            <option value="{{$unit->id}}">{{$unit->short_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name='rate[]' id="{{'rate'.$i}}" placeholder='Rate' value="{{old('rate.'.$i)}}" class="form-control"/></td>
                                                <td><input type="text" name='discount[]' placeholder='Discount' value="{{old('discount.'.$i)}}" class="form-control"/></td>
                                                <td><input type="text" name='amt[]' placeholder='Amount' value="{{old('amt.'.$i)}}" class="form-control" readonly /></td>
                                                <td align="center"><a href="javascript:void(0);" data-toggle="delete-row"><span class="btn btn-red btn-circle"><i class="glyphicon glyphicon-trash"></i></span></a></td>
                                            </tr>
                                        @endfor
                                        <tr id="{{'addr'.$i}}"></tr>
                                        <input type="hidden" id="invoice_lines_count" value="{{$i}}"/>
                                        
                                    @else
                                        @php $counter = 1; @endphp
                                        @if (isset($invoiceHeader))
                                            @foreach ($invoiceHeader->invoice_lines as $key => $invoiceLines)
                                                @php $counter = $key; @endphp
                                                <tr id='addr{{$key}}'>
                                                    <td>{{$key+1}}</td>
                                                    <td>
                                                        <select name='description[]' id="description{{$key}}" class='form-control' style='width: 100%'>
                                                            <option value>Select Item</option>
                                                            @foreach ($items as $item)
                                                                @if ($invoiceLines->item_id == $item->id)
                                                                    <option selected value="{{$item->id}}" data-hsn="{{$item->hsn_code}}">{{$item->description}}</option>
                                                                @else
                                                                    <option value="{{$item->id}}" data-hsn="{{$item->hsn_code}}">{{$item->description}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>

                                                    <td><input type="text" name='hsn_code[]' id="hsn_code{{$key}}" placeholder='HSN Code' class="form-control" readonly value="{{$invoiceLines->item->hsn_code}}" /></td>

                                                    <td><input type="text" name='qty[]' id="qty{{$key}}" placeholder='Qty' class="form-control" value="{{$invoiceLines->quantity}}" /></td>
                                                    <td>
                                                        <select name='unit[]' id="unit{{$key}}" class="form-control unit">
                                                            @foreach ($units as $unit)
                                                                @if ($invoiceLines->unit_id == $unit->id)
                                                                    <option selected value="{{$unit->id}}">{{$unit->short_name}}</option>
                                                                @else
                                                                    <option value="{{$unit->id}}">{{$unit->short_name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><input type="text" name='rate[]' id="rate{{$key}}" placeholder='Rate' class="form-control" value="{{$invoiceLines->rate}}" /></td>
                                                    <td><input type="text" name='discount[]' placeholder='Discount' class="form-control" value="{{$invoiceLines->discount}}" /></td>
                                                    <td><input type="text" name='amt[]' placeholder='Amount' class="form-control" readonly value="{{$invoiceLines->amount}}" /></td>
                                                    <td align="center"><a href="javascript:void(0);" data-toggle="delete-row"><span class="btn btn-red btn-circle"><i class="glyphicon glyphicon-trash"></i></span></a></td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr id='addr0'>
                                                <td>1</td>
                                                <td>
                                                    <select name='description[]' id="description0" class='form-control' style='width: 100%'>
                                                        <option value>Select Item</option>
                                                        @foreach ($items as $item)
                                                            <option value="{{$item->id}}" data-hsn="{{$item->hsn_code}}">{{$item->description}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name='hsn_code[]' id="hsn_code0" placeholder='HSN Code' class="form-control" readonly /></td>
                                                <td><input type="text" name='qty[]' id="qty0" placeholder='Qty' class="form-control"/></td>
                                                <td>
                                                    <select name='unit[]' id="unit0" class="form-control unit">
                                                        @foreach ($units as $unit)
                                                            <option value="{{$unit->id}}">{{$unit->short_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td><input type="text" name='rate[]' id="rate0" placeholder='Rate' class="form-control"/></td>
                                                <td><input type="text" name='discount[]' placeholder='Discount' class="form-control"/></td>
                                                <td><input type="text" name='amt[]' placeholder='Amount' class="form-control" readonly /></td>
                                                <td align="center"><a href="javascript:void(0);" data-toggle="delete-row"><span class="btn btn-red btn-circle"><i class="glyphicon glyphicon-trash"></i></span></a></td>
                                            </tr>
                                        @endif
                                        
                                        <tr id='addr{{$counter+1}}'></tr>
                                        <input type="hidden" id="invoice_lines_count" value="{{$counter+1}}"/>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <a id="add_row" class="btn btn-default pull-left top-20" accesskey="a">Add Item</a>
                    {{-- <a id='delete_row' class="pull-right btn btn-default top-20" accesskey="l">Delete Item</a> --}}
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Total & Tax Details</div>
                <div class="panel-body">
                    <div class="row">
                    <div class="form-group">
                        <label class="col-lg-3" for="invoice_total">Invoice Total</label>
                        <div class="col-lg-8">
                            <input type="text" name="invoice_total" id="invoice_total" placeholder="Invoice Total" readonly class="form-control" @isset($invoiceHeader) value="{{$invoiceHeader->invoice_total}}" @endisset>
                        </div>
                    </div>
                    </div>
                    
                    <div class="row">
                            <div class="col-lg-offset-1 col-lg-9 input-group" style="top:20px;margin-bottom:20px">
                              <span class="input-group-addon" style="font-weight:bolder">CGST @ </span>
                              <input type="text" class="form-control" placeholder="Percentage" name="cgst_percent" id="cgst_percent" data-tax-type-per="cgst" @isset($invoiceHeader) value="{{$invoiceHeader->cgst_percent}}" @endisset>
                              <span class="input-group-addon">%</span>
                              <span class="input-group-addon">=</span>
                              <input type="text" class="form-control" placeholder="Amount" name="cgst_amount" id="cgst_amount" readonly data-tax-type-amt="cgst" @isset($invoiceHeader) value="{{$invoiceHeader->cgst_amount}}" @endisset>
                            </div>
                            <div class="col-lg-2"></div>
                    </div>
                    <div class="row">
                            <div class="col-lg-offset-1 col-lg-9 input-group" style="top:20px;margin-bottom:20px">
                              <span class="input-group-addon" style="font-weight:bolder">SGST @ </span>
                              <input type="text" class="form-control" placeholder="Percentage" name="sgst_percent" id="sgst_percent" data-tax-type-per="sgst"  @isset($invoiceHeader) value="{{$invoiceHeader->sgst_percent}}" @endisset>
                              <span class="input-group-addon">%</span>
                              <span class="input-group-addon">=</span>
                              <input type="text" class="form-control" placeholder="Amount" readonly name="sgst_amount" id="sgst_amount" data-tax-type-amt="sgst" @isset($invoiceHeader) value="{{$invoiceHeader->sgst_amount}}" @endisset>
                            </div>
                            <div class="col-lg-2"></div>
                    </div>
                    <div class="row">
                            <div class="col-lg-offset-1 col-lg-9 input-group" style="top:20px;margin-bottom:20px">
                              <span class="input-group-addon" style="font-weight:bolder">IGST @ </span>
                              <input type="text" class="form-control" placeholder="Percentage" @isset($invoiceHeader) value="{{$invoiceHeader->igst_percent}}" @endisset name="igst_percent" id="igst_percent" data-tax-type-per="igst">
                              <span class="input-group-addon">%</span>
                              <span class="input-group-addon">=</span>
                              <input type="text" class="form-control" placeholder="Amount" readonly @isset($invoiceHeader) value="{{$invoiceHeader->igst_amount}}" @endisset name="igst_amount" id="igst_amount" data-tax-type-amt="igst">
                            </div>
                            <div class="col-lg-2"></div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="round_off">Round Off</label>
                        <div class="col-lg-8">
                            <input type="text" name="round_off" id="round_off" placeholder="Round Off" readonly class="form-control"  @isset($invoiceHeader) value="{{$invoiceHeader->round_off}}" @endisset>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-3" for="grand_total">Grand Total</label>
                        <div class="col-lg-8">
                            <input type="text" name="grand_total" id="grand_total" placeholder="Grand Total" readonly class="form-control" @isset($invoiceHeader) value="{{$invoiceHeader->grand_total}}" @endisset>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-lg-3" for="amount_in_words">Amount In Words</label>
                        <div class="col-lg-8">
                            <input type="text" name="amount_in_words" id="amount_in_words" placeholder="Amount In Words" readonly class="form-control" @isset($invoiceHeader) value="{{$invoiceHeader->amount_in_words}}" @endisset>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="panel panel-default">
                <div class="panel-body" align="center">
                    <button type="button" name="save" class="btn btn-success" id="save" onclick="set_next_action(this.id)" accesskey="s">Save Invoice</button>
                    <button type="button" name="save_preview" id="save_preview" class="btn btn-info" onclick="set_next_action(this.id)" accesskey="r">Save & Preview</button>
                    <button type="button" name="save_print" class="btn btn-warning" id="save_print" onclick="set_next_action(this.id)" accesskey="p">Save & Print</button>
                    <input type="hidden" name="buttonType" id="buttonType"/>
                </div>
            </div>
        </div>
    </div>
    </form>
</div>


@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery-ui.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.min.js') }}"></script>

<script type="text/javascript">
    function showProcessing(processingText="Please wait...") {
        $("#loader_text").text(processingText);
        $("#processing_image.overlay").css({"visibility" :"visible", "opacity" :"1"});
    }
    
    function stopProcessing() {
        $("#processing_image.overlay").css({"visibility" :"hidden", "opacity" :"0"});
    }
</script>


<script type="text/javascript">
/* global $ */
$(function(){
   
    // for converting into datepicker
    $('#invoice_date').datepicker({
        "dateFormat" : 'dd/mm/yy',
        "width" : "1em"
    }).datepicker("setDate", $("#invoice_date").attr("rel"));


    //var to detect new item added to select2 list.
    var newOption = false;
    //for converting into select2
    $('.buyer_list').select2({
      selectOnClose: true,
      tags: true,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true
        }
      },
      templateResult: function (data) {
        var $result = $("<span></span>");

        $result.text(data.text);

        if (data.newOption) {
          $result.append(" <em>(new)</em>");
        }

        return $result;
      }
    }).on('select2:select', function(e) {
        //for capturing if new item is added to the select2 list.
        if(e.params.data.newOption || (e.params.data.id != "" && isNaN(e.params.data.id))) {
            $("#gstn").removeAttr("readonly");
            $("#address").removeAttr("readonly");
            $("#address_2").removeAttr("readonly");
            $("#mobile").removeAttr("readonly");
            $("#state").removeAttr("readonly"); 
            
            var form_data = "@if (Cookie::get('sales_form_new_buyer') !== null) {{(Cookie::get('sales_form_new_buyer'))}}  @endif";
            var data = form_data.split(";");
            
            if (data[0].trim() == e.params.data.id) {
                $("#gstn").val(data[1]).focus();         
                $("#address").val(data[2]);
                $("#address_2").val(data[3]);
                $("#mobile").val(data[4]);
                $("#state").val(data[5]);    
            } else {
                $("#gstn").val('').focus();         
                $("#address").val('');
                $("#address_2").val('');
                $("#mobile").val('');
                $("#state").val('');
                
                
            }
            
            
        } else {
            var address = $("[name="+$(this).prop("name")+"] option:selected").data("address");
            var address_2 = $("[name="+$(this).prop("name")+"] option:selected").data("address_2");
            var mobile = $("[name="+$(this).prop("name")+"] option:selected").data("mobile");
            var gstn = $("[name="+$(this).prop("name")+"] option:selected").data("gstn");
            var state = $("[name="+$(this).prop("name")+"] option:selected").data("state");

            if (address) {
                $("#gstn").prop("readonly", "true").prop("value", gstn);
                $("#address").prop("readonly", "true").prop("value", address);
                $("#address_2").prop("readonly", "true").prop("value", address_2);
                $("#mobile").prop("readonly", "true").prop("value", mobile);
                $("#state").prop("readonly", "true").prop("value", state); 
            } else {
                $("#gstn").prop("readonly", "true").prop("value", '');
                $("#address").prop("readonly", "true").prop("value", '');
                $("#address_2").prop("readonly", "true").prop("value", '');
                $("#mobile").prop("readonly", "true").prop("value", '');
                $("#state").prop("readonly", "true").prop("value", ''); 
            }
            $("#description0").select2("open");
        }
    })

    $("[name^=description]").select2({
      selectOnClose: true,
      tags: true,
      createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true
        }
      },
      templateResult: function (data) {
        var $result = $("<span></span>");

        $result.text(data.text);

        if (data.newOption) {
          $result.append(" <em>(new)</em>");
        }

        return $result;
      }
    }).on('select2:select', function(e) {
        //for capturing if new item is added to the select2 list.
        if(e.params.data.newOption) {
            $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").removeAttr("readonly");
            $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").val("");
            $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").focus();
        } else {
            $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("readonly", true);
            $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=qty]").focus();
            var hsn = $("[id="+$(this).prop("id")+"] option:selected").data("hsn");
            if (hsn)
                $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("value", hsn);
            else
                $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("value", "");
        }
    });

    $("[name=transport]").select2({
    selectOnClose: true,
    tags: true,
    createTag: function (params) {
        return {
          id: params.term,
          text: params.term,
          newOption: true
        }
    },
    templateResult: function (data) {
        var $result = $("<span></span>");

        $result.text(data.text);

        if (data.newOption) {
          $result.append(" <em>(new)</em>");
        }

        return $result;
      }
    }).on('select2:select', function(e) {
        //for capturing if new item is added to the select2 list.
        if(e.params.data.newOption) {
            
        } else {
            
        }
    });     
});


//for adding and deleting item from invoice lines table.
$(document).ready(function(){
    var i=parseInt($("#invoice_lines_count").val());
    $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><select name='description[]' id='description"+i+"' class='form-control'><option value>Select Item</option>@foreach ($items as $item) <option value='{{$item->id}}' data-hsn='{{$item->hsn_code}}'>{{$item->description}}</option> @endforeach</select></td><td><input  name='hsn_code[]' id='hsn_code"+i+"' type='text' placeholder='HSN Code'  class='form-control input-md' readonly></td><td><input  name='qty[]' id='qty"+i+"' type='text' placeholder='Qty'  class='form-control input-md'></td><td><select name='unit[]' id='unit"+i+"' class='form-control'>@foreach ($units as $unit) <option value='{{$unit->id}}'>{{$unit->short_name}}</option> @endforeach</select></td><td><input  name='rate[]' id='rate"+i+"'  type='text' placeholder='Rate'  class='form-control input-md'></td><td><input  name='discount[]' id='discount"+i+"' type='text' placeholder='Discount'  class='form-control input-md'></td><td><input  name='amt[]' id='amt"+i+"' type='text' placeholder='Amount'  class='form-control input-md' readonly></td><td align='center'><a href='javascript:void(0);' data-toggle='delete-row'><span class='btn btn-red btn-circle'><i class='glyphicon glyphicon-trash'></i></span></a></td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
      
      
      $("[name^=description]").select2({
          selectOnClose: true,
          tags: true,
          createTag: function (params) {
            return {
              id: params.term,
              text: params.term,
              newOption: true
            }
          },
          templateResult: function (data) {
            var $result = $("<span></span>");

            $result.text(data.text);

            if (data.newOption) {
              $result.append(" <em>(new)</em>");
            }

            return $result;
          }
        }).on('select2:select', function(e) {
            //for capturing if new item is added to the select2 list.
            if(e.params.data.newOption) {
                $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").removeAttr("readonly");
            } else {
                $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("readonly", true);
                var hsn = $("[id="+$(this).prop("id")+"] option:selected").data("hsn");
                if (hsn)
                    $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("value", hsn);
                else
                    $("[id="+$(this).prop("id")+"]").parent().siblings().find("[name^=hsn_code]").prop("value", "");
            }
        });
    });
    $("#delete_row").click(function(){
        if(i>1) {
            $("#addr"+(i-1)).html('');
            i--;
            calculate_invoice_total();
            calculate_grand_total();
        }
        
    });
});


//calcuate total, taxes and grand total when rate or discount change.
$(function(){
    
    $(document).on('keydown', '[name^=qty]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    $(document).on('keydown', '[name^=rate]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});    
    $(document).on('keydown', '[name^=discount]', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    
    $(document).on('change', '[name^=qty]', function(){
        var qty = $(this).val();
        
        var rate = $(this).parent().siblings().find('[name^=rate]').val();
        if (!rate) {
            return;
        }
        
        var discount = $(this).parent().siblings().find('[name^=discount]').val();
        if(!discount) {
            discount = 0;
        }

        var amount = (parseFloat(qty) * parseFloat(rate));
        
        amount = amount - ((discount / 100) * amount);
        
        
        !isNaN(amount) && amount != 0 ? $(this).parent().siblings().find('[name^=amt]').val(amount.toFixed(2)) : $(this).parent().siblings().find('[name^=amt]').val('');

        calculate_invoice_total();
        
        calculate_grand_total();        
    });

    $(document).on('change', '[name^=rate]', function(){
        var qty = $(this).parent().siblings().find('[name^=qty]').val();
        if (!qty) {
            return;
        }

        var rate = $(this).val();

        var discount = $(this).parent().siblings().find('[name^=discount]').val();
        if(!discount) {
            discount = 0;
        }

        var amount = (parseFloat(qty) * parseFloat(rate));
        
        amount = amount - ((discount / 100) * amount);

        !isNaN(amount) && amount != 0 ? $(this).parent().siblings().find('[name^=amt]').val(amount.toFixed(2)) : $(this).parent().siblings().find('[name^=amt]').val('');

        calculate_invoice_total();
        
        calculate_grand_total();
    });

    $(document).on('change', '[name^=discount]', function(){
        var qty = $(this).parent().siblings().find('[name^=qty]').val();
        if (!qty) {
            return;
        }

        var rate = $(this).parent().siblings().find('[name^=rate]').val();
        if (!rate) {
            return;
        }

        var discount = $(this).val();

        var amount = (parseFloat(qty) * parseFloat(rate));
        
        amount = amount - ((discount / 100) * amount);

        !isNaN(amount) && amount != 0 ? $(this).parent().siblings().find('[name^=amt]').val(amount.toFixed(2)) : $(this).parent().siblings().find('[name^=amt]').val('');

        calculate_invoice_total();
        
        calculate_grand_total();
        
    });
});

function calculate_invoice_total(){
    var invoice_total = 0.0;
    $('[name^=amt]').each(function(key, value){
        var this_amt = parseFloat($(this).val());
        invoice_total += this_amt;
    });
    
    !isNaN(invoice_total) && invoice_total != 0.0 ? $("#invoice_total").val(invoice_total.toFixed(2)) : $("#invoice_total").val('');
}

function calculate_grand_total(){
    var invoice_total = parseFloat($("#invoice_total").val());
    
    var cgst_percent = parseFloat($("#cgst_percent").val());
    var sgst_percent = parseFloat($("#sgst_percent").val());
    var igst_percent = parseFloat($("#igst_percent").val());
    
    var cgst_amount = 0.0;
    var sgst_amount = 0.0;
    var igst_amount = 0.0;
    
    if(!isNaN(cgst_percent) && !isNaN(invoice_total) && cgst_percent != "") {
        cgst_amount = parseFloat(((cgst_percent / 100) * invoice_total).toFixed(2));
    }
    
    if(!isNaN(sgst_percent) && !isNaN(invoice_total) && sgst_percent != "") {
        sgst_amount = parseFloat(((sgst_percent / 100) * invoice_total).toFixed(2));
    }
    
    if(!isNaN(igst_percent) && !isNaN(invoice_total) && igst_percent != "") {
        igst_amount = parseFloat(((igst_percent / 100) * invoice_total).toFixed(2));
    }
    
    var intermediate_total = invoice_total + cgst_amount + sgst_amount + igst_amount;

    var grand_total = Math.floor(intermediate_total);

    var round_off = (intermediate_total - grand_total).toFixed(2);

    !isNaN(cgst_amount) && cgst_amount != 0.0 ? $("[name='cgst_amount']").val(cgst_amount) : $("[name='cgst_amount']").val('');
    !isNaN(sgst_amount) && sgst_amount != 0.0 ? $("[name='sgst_amount']").val(sgst_amount) : $("[name='sgst_amount']").val('');
    !isNaN(igst_amount) && igst_amount != 0.0 ? $("[name='igst_amount']").val(igst_amount) : $("[name='igst_amount']").val('');
    !isNaN(round_off) ? $("#round_off").val(round_off) : $("#round_off").val('');
    
    if(!isNaN(grand_total) && grand_total != 0.0) {
        $("#grand_total").val(grand_total);
        $("#amount_in_words").val(amount_in_words(parseInt(grand_total)));
    } else {
        $("#grand_total").val('');
        $("#amount_in_words").val('');
    }
}

var a = ['','one ','two ','three ','four ', 'five ','six ','seven ','eight ','nine ','ten ','eleven ','twelve ','thirteen ','fourteen ','fifteen ','sixteen ','seventeen ','eighteen ','nineteen '];
var b = ['', '', 'twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety'];

function amount_in_words (num) {
    if ((num = num.toString()).length > 9) return 'overflow';
    var n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return; var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + 'only ' : '';
    return str;
}

function reset_tax () {
    $("#invoice_total").val('');
    $("[name='cgst_amount']").val('');
    $("[name='sgst_amount']").val('');
    $("[name='igst_amount']").val('');
    $("#round_off").val('');
    $("#grand_total").val('');
    $("#amount_in_words").val('');
}

function set_next_action(element) {
    
    $("#buttonType").val(element);
    
    if (isNaN($(".buyer_list").val())) {
        $.ajax({
            url: "{{route('save_form_data')}}",
            data: {
                "buyer"    : $(".buyer_list").val(),
                "gstn"     : $("#gstn").val(),
                "address"  : $("#address").val(),
                "address_2": $("#address_2").val(),
                "mobile"   : $("#mobile").val(),
                "state"    : $("#state").val()
            },
            dataType: "json",
            method: "get",
            beforeSend: function() {
                showProcessing("Saving Form Data...");
            },
            complete: function(response) {
                showProcessing("Submitting Form...");
                console.log ("hello");
                $("form").submit();
                
            }
        }); 
    } else {
        //this space is to perform any action if any before submitting form....
    
    
        $("form").submit();
    }
    return false;
    
}

$(function() {
   $('input[data-tax-type-per]').on("keydown", function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
    
   $("input[data-tax-type-per]").on("keyup", function(){
      var tax_type = $(this).data("tax-type-per");
      var tax_percent = $(this).val();
      var invoice_total = $("#invoice_total").val();
      var tax_amount = 0.0;
      
      if(!isNaN(tax_percent) && !isNaN(invoice_total) && tax_percent != "") {
          tax_percent = parseFloat(tax_percent);
          invoice_total = parseFloat(invoice_total);
          tax_amount = (invoice_total / 100) * tax_percent;
      }
      
      tax_amount = parseFloat(tax_amount.toFixed(2));
      
      if (!isNaN(tax_amount) && tax_amount != 0) 
        $("input[data-tax-type-amt='"+tax_type+"']").val(tax_amount);
      else
        $("input[data-tax-type-amt='"+tax_type+"']").val('');
      
      calculate_grand_total();
   });

   $(document).on ("click", "[data-toggle='delete-row']", function(){
        $(this).parents("tr").remove();
   });
});

</script>

<script type="text/javascript">
    $(function(){
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });
        
        $(document).on('keypress', 'input,select', function (e) {
            if (e.which == 13) {
                e.preventDefault();
                // Get all focusable elements on the page
                var $canfocus = $(':focusable');
                var index = $canfocus.index(document.activeElement) + 1;
                if (index >= $canfocus.length) index = 0;
                $canfocus.eq(index).focus();
            }
        });
    });
</script>
@endsection