<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GSTSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('gst')->insert([
                "id" => 1,
                "name" => "CGST",
                "percent" => 5.0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        DB::table('gst')->insert([
                "id" => 2,
                "name" => "SGST",
                "percent" => 5.0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
        ]);
        DB::table('gst')->insert([
                "id" => 3,
                "name" => "IGST",
                "percent" => 5.0,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
        ]);
        
    }
}
