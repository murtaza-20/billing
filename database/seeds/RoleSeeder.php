<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table("roles")->insert([
            "id"           => 1,
            "name"         => "owner",
            "display_name" => "Project Owner",
            "description"  => "owner of the project.",
            "created_at"   => Carbon::now(),
            "updated_at"   => Carbon::now()
        ]);
        
        DB::table("roles")->insert([
            "id"           => 2,
            "name"         => "admin",
            "display_name" => "Admin",
            "description"  => "allowed to add, update and delete buyers, items and units.",
            "created_at"   => Carbon::now(),
            "updated_at"   => Carbon::now()
        ]);
    }
}
