<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('units')->insert([
                "id" => 1,
                "name" => "Each",
                "short_name" => "Each",
                "sort_order" => 5,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 2,
                "name" => "Kilo Gram",
                "short_name" => "Kgs.",
                "sort_order" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 3,
                "name" => "Gram",
                "short_name" => "Gms",
                "sort_order" => 2,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 4,
                "name" => "Dozen",
                "short_name" => "Dzn",
                "sort_order" => 4,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 5,
                "name" => "Feet",
                "short_name" => "Ft.",
                "sort_order" => 6,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 6,
                "name" => "Inches",
                "short_name" => "Inch.",
                "sort_order" => 7,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
        
        DB::table('units')->insert([
                "id" => 7,
                "name" => "Set",
                "short_name" => "Set",
                "sort_order" => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
    }
}
