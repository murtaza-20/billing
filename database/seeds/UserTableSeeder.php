<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
                "id" => 1,
                "name" => "Test User",
                "email" => "billing@malrapp.com", 
                "password" => bcrypt("Admin123"),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );
    }
}