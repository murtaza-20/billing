<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvoiceLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('invoice_lines', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('invoice_header_id')->unsigned()->nullable();
            $table->integer('item_id')->unsigned()->nullable();
            $table->float('quantity', 5, 1)->nullable();
            $table->float('rate', 10, 2)->nullable();
            $table->float('discount', 5, 2)->nullable();
            $table->double('amount', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('invoice_lines');
    }
}
