<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransportColInvHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("invoice_header", function(Blueprint $table){
            $table->dropColumn("transport");
            $table->integer("transport_id")->unsigned()->nullable()->after("invoice_date");
            $table->foreign('transport_id')->references("id")->on("transports")->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("invoice_header");
    }
}
