<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablesAddForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('invoice_header', function($table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('buyer_id')->references('id')->on('buyers')->onDelete('cascade');
        });

        Schema::table('invoice_lines', function($table) {
            $table->foreign('invoice_header_id')->references('id')->on('invoice_header')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('invoice_header', function($table) {
            $table->dropForeign('invoice_header_user_id_foreign');
            $table->dropForeign('invoice_header_buyer_id_foreign');
        });

        Schema::table('invoice_lines', function($table) {
            $table->dropForeign('invoice_lines_invoice_header_id_foreign');
            $table->dropForeign('invoice_lines_item_id_foreign');
        });
    }
}
