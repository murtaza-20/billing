<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvoiceHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('invoice_header', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('buyer_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('invoice_number')->nullable();
            $table->date('invoice_date')->nullable();
            $table->string('transport')->nullable();
            $table->string('challan_number')->nullable();
            $table->double('invoice_total', 20, 2)->nullable();
            $table->float('cgst_percent', 5, 2)->nullable();
            $table->double('cgst_amount', 20, 2)->nullable();
            $table->float('sgst_percent', 5, 2)->nullable();
            $table->double('sgst_amount', 20, 2)->nullable();
            $table->float('igst_percent', 5, 2)->nullable();
            $table->double('igst_amount', 20, 2)->nullable();
            $table->float('round_off', 5, 2)->nullable();
            $table->double('grand_total', 20, 2)->nullable();
            $table->string('amount_in_words')->nullable();
            $table->string('slug')->nullable();
            $table->enum('invoice_status', ['Approved', 'Rejected'])->default('Approved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('invoice_header');
    }
}
