<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActivateToBuyerItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table("buyers", function($table){
            $table->string("email_address")->nullable()->after("mobile");
            $table->string("city")->nullable()->after("gstn");
            $table->enum("status", ['Activated', 'De-Activated'])->default('Activated')->after("state");
        });
        Schema::table("items", function($table){
            $table->enum("status", ['Activated', 'De-Activated'])->default('Activated')->after("hsn_code");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists("buyers");
        Schema::dropIfExists("items");
    }
}
