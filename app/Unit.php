<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
    protected $table = 'units';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'short_name', 'status', 'sort_order', 'created_at', 'updated_at'];
    
    public static function filter ($column="id", $orderby="desc", $no_of_record=10, $search="") {
		if (!empty($search)) {
			$result = self::where('name', 'LIKE', "%".$search."%")
							->orWhere('short_name', 'LIKE', "%".$search."%")
							->orWhere('sort_order', 'LIKE', "%".$search."%")
							->orWhere('status', 'LIKE', "%".$search."%")
							->orderBy($column, $orderby)
							->paginate($no_of_record);
			return $result;
		} else {
			$result = self::orderBy($column, $orderby)->paginate($no_of_record);
			return $result;
		}
	}
}
