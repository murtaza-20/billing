<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    protected $table = "items";
    protected $primaryKey = "id";
    protected $fillable = ["description", "hsn_code", "created_at", "updated_at"];

    public static function filter ($column="id", $orderby="desc", $no_of_record=10, $search="") {
		if (!empty($search)) {
			$result = self::where('description', 'LIKE', "%".$search."%")
							->orWhere('hsn_code', 'LIKE', "%".$search."%")
							->orWhere('status', 'LIKE', "%".$search."%")
							->orderBy($column, $orderby)
							->paginate($no_of_record);
			return $result;
		} else {
			$result = self::orderBy($column, $orderby)->paginate($no_of_record);
			return $result;
		}
	}

	public function invoiceLine () {
		return $this->belongsTo ("App\InvoiceLines", "id");
	}
}
