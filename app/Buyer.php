<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buyer extends Model
{
    //
	protected $table = "buyers";
	protected $primaryKey = "id";
	protected $fillable = ["name", "address", "address_2", "mobile", "email_address", "gstn", "city", "state", "status", "created_at", "updated_at"];
	
	public static function filter ($column="id", $orderby="desc", $no_of_record=10, $search="") {
		if (!empty($search)) {
			$result = self::where('name', 'LIKE', "%".$search."%")
							->orWhere('address', 'LIKE', "%".$search."%")
							->orWhere('address_2', 'LIKE', "%".$search."%")
							->orWhere('mobile', 'LIKE', "%".$search."%")
							->orWhere('email_address', 'LIKE', "%".$search."%")
							->orWhere('gstn', 'LIKE', "%".$search."%")
							->orWhere('city', 'LIKE', "%".$search."%")
							->orWhere('state', 'LIKE', "%".$search."%")
							->orWhere('status', 'LIKE', "%".$search."%")
							->orderBy($column, $orderby)
							->paginate($no_of_record);
			return $result;
		} else {
			$result = self::orderBy($column, $orderby)->paginate($no_of_record);
			return $result;
		}
	}
}