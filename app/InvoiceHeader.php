<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class InvoiceHeader extends Model
{
    protected $table = "invoice_header";
	protected $primaryKey = "id";
	protected $fillable = ["user_id", "buyer_id", "invoice_number", "invoice_date", "transport_id", "challan_number", "invoice_total", "cgst_percent", "cgst_amount", "sgst_percent", "sgst_amount", "igst_percent", "igst_amount", "round_off", "grand_total", "amount_in_words", "slug", "created_at", "updated_at"];

	public function buyer () {
		return $this->belongsTo ("App\Buyer");
	}

	public function user () {
		return $this->belongsTo ("App\User");
	}
	
	public function transport () {
		return $this->belongsTo ("App\Transport");
	}
	
	public function invoice_lines () {
		return $this->hasMany ("App\InvoiceLines");
	}
	
	public static function getBySlug ($slug = '') {
		if (!empty($slug)) {
			return self::where('slug', $slug)->first();
		} else {
			return NULL;
		}
	}

	public static function filter ($column="invoice_number", $orderby="desc", $no_of_record=10, $search="") {
		if (!empty($search)) {
			$result = self::select("invoice_header.*", "buyers.name")
							->leftJoin("buyers", "buyers.id", "=", "buyer_id")
							->where('invoice_number', 'LIKE', "%".$search."%")
							->orWhere('invoice_date', 'LIKE', "%".$search."%")
							->orWhere('buyers.name', 'LIKE', "%".$search."%")
							->orWhere('transport', 'LIKE', "%".$search."%")
							->orWhere('invoice_total', 'LIKE', $search)
							->orWhere('cgst_percent', 'LIKE', $search)
							->orWhere('cgst_amount', 'LIKE', $search)
							->orWhere('sgst_percent', 'LIKE', $search)
							->orWhere('sgst_amount', 'LIKE', $search)
							->orWhere('igst_percent', 'LIKE', $search)
							->orWhere('igst_amount', 'LIKE', $search)
							->orWhere('grand_total', 'LIKE', $search)
							->orderBy($column, $orderby)
							->paginate($no_of_record);
			return $result;
		} else {
			$result = self::orderBy($column, $orderby)->paginate($no_of_record);
			return $result;
		}
	}
}
