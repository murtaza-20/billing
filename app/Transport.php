<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    //
    protected $table = "transports";
    protected $primaryKey = "id";
    protected $fillable = ["transport_name", "city", "created_at", "updated_at"];
}
