<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

use Auth;
use Session;
use Log;

class SalesInvoiceSave extends FormRequest
{
    
    public function __construct(ValidationFactory $validationFactory) {

        $validationFactory->extend(
            'checkscripts',
            function ($attribute, $value, $parameters) {
                $temp = strip_tags($value);
                return !strcmp($temp, $value);
            },
            'Invalid/Illegal input in :attribute. Please remove any HTML or SCRIPT tags.'
        );

    }
    
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        if (Auth::check())
            return true;
        else
            return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'buyer_id'       => 'required|checkscripts',
            'transport'      => 'checkscripts',
            'challan_number' => 'checkscripts',
            'gstn'           => 'checkscripts',
            'address'        => 'checkscripts',
            'address_2'      => 'checkscripts',
            'mobile'         => 'checkscripts',
            'state'          => 'checkscripts'
        ];
        
        $invoice_lines_count = 0;
        
        foreach($this->request->get('description') as $key => $val) {
            $rules['description.'.$key] = 'required';
            $invoice_lines_count++;
            Session::put('description.'.$key, $val);
            Log::info ("description $key : ". Session::get("description.".$key));
        }
        
        foreach($this->request->get('qty') as $key => $val) {
            $rules['qty.'.$key] = 'required';
        }
        
        foreach($this->request->get('rate') as $key => $val) {
            $rules['rate.'.$key] = 'required';
        }
        
        foreach($this->request->get('hsn_code') as $key => $val) {
            $rules['hsn_code.'.$key] = 'checkscripts';
        }
        
        Session::put('buyer', $this->request->get('buyer_id'));
        Session::put('transport', $this->request->get('transport'));
        Session::put("invoice_lines_count", $invoice_lines_count);
        
        
        return $rules;
    }
    
    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages() {
        $messages = [
            'buyer_id.required' => "Buyer is required.",
            'buyer_id.checkscripts' => "Invalid/Illegal input in buyer name. Please remove any HTML or SCRIPT tags."
        ];
        
        
        
        foreach($this->request->get('description') as $key => $val) {
            $messages['description.'.$key.'.required'] = "Description ".($key+1)." is required.";
        }
        
        foreach($this->request->get('qty') as $key => $val) {
            $messages['qty.'.$key.'.required'] = "Quantity ".($key+1)." is required. If nothing, put a 0 (zero).";
        }
        
        foreach($this->request->get('rate') as $key => $val) {
            $messages['rate.'.$key.'.required'] = "Rate ".($key+1)." is required. If nothing, put a 0 (zero).";
        }
        
        foreach($this->request->get('hsn_code') as $key => $val) {
            $messages['hsn_code.'.$key.'.checkscripts'] = 'Invalid/Illegal input in HSN Code '.($key + 1).'. Please remove any HTML or SCRIPT tags.';
        }
        
        return $messages;
    }
}
