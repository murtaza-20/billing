<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buyer;
use App\User;

use Response;
use Auth;

class BuyerController extends Controller
{
    //
    public function buyers () {
        $buyers = Buyer::filter();
        $buyers_dashboard = view("layouts.buyers", compact("buyers"))->render();
        
        if (session()->has("excel_upload_exception")) 
            session()->forget("excel_upload_exception");
            
        return view("buyers", compact("buyers_dashboard"));
    }
    
    public function sort_buyer (Request $request) {
        $column           = $request->get('column') ? $request->get('column') : "id";
        $orderby          = $request->get('orderby') ? $request->get('orderby') : "desc";
        $no_of_record     = $request->get('no_of_record');
        $search           = $request->get('search');
        $user             = User::where("id", Auth::user()->id)->first();
        $msg              = $user["name"];
        $buyers           = Buyer::filter($column, $orderby, $no_of_record, $search);
        $buyers_dashboard = view("layouts.buyers", compact("buyers"))->render();
        $response         = array("msg" => "success", "html" => $buyers_dashboard);
        return response()->json($response);
    }
    
    public function download_bulk_import_buyer_template () {
        return Response::download(storage_path('bulk_import/buyer_upload_billing.xls'));
    }
}
