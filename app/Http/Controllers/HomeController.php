<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helper\Helper;

use App\User;
use App\GST;
use App\Item;
use App\Buyer;
use App\InvoiceHeader;
use App\InvoiceLines;
use App\Unit;
use App\Transport;

use Carbon\Carbon;
use App\Http\Requests\SalesInvoiceSave;

use Auth;
use DB;
use Session;
use Excel;
use Zipper;
use Response;
use File;
use PDF;
use Cookie;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $msg = "";
        $invoices = "";
        $dasboard = "";
        if (Auth::check()) {
            $user = User::where("id", Auth::user()->id)->first();    
            $msg = $user["name"];
            $invoices     = InvoiceHeader::filter();
            $dashboard    = view("layouts.dashboard", compact("invoices"))->render();
            
            
            
            
            
            return view('home', compact("msg", "dashboard"));
            
        } else {
            $msg = "Guest";
        }
    
        return view('home', compact("msg", "dashboard"));
    }

    /**
     * Show the sales invoice form.
     *
     * @return \Illuminate\Http\Response
     */
    public function sales ($invoiceSlug = "") {

        $invoiceHeader = null;
        if ($invoiceSlug != "") {
            $invoiceHeader = InvoiceHeader::where("slug", $invoiceSlug)->first();
            if (empty($invoiceHeader)) {
                abort(404, "Invoice not found..!!!");
            }
        }

        //send all the items in the db.
        $items = Item::where("status", "Activated")->get(['id', 'description', 'hsn_code']);
        
        //send all the buyers in the db.
        $buyers = Buyer::where("status", "Activated")->get(['id', 'name', 'gstn', 'address', 'address_2', 'state', 'mobile']);
        
        //send all the available units.
        $units = Unit::orderBy("sort_order")->get(["id", "name", "short_name"]);
        
        //send all the available transports.
        $transports = transport::get(["id", "transport_name", "city"]);
        
        //get the last entered invoice number and calculate the next one.
        $last_invoice = InvoiceHeader::orderBy('id', 'desc')->first(['id', 'invoice_number']);
        $next_invoice_number = 0;

        if (!empty($last_invoice)) {
            $last_invoice_number = (int)$last_invoice['invoice_number'];
            $next_invoice_number = sprintf('%03d', $last_invoice_number + 1);
        } else {
            $next_invoice_number = sprintf('%03d', 1);
        }
        if ($invoiceHeader != null)
            return view('sales', compact('items', 'buyers', 'next_invoice_number', 'units', 'transports', 'invoiceHeader'));
        else
            return view('sales', compact('items', 'buyers', 'next_invoice_number', 'units', 'transports'));
    }

    /**
     * Display a print view of the given invoice ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function printview(Request $request, $invoice_slug='') {
        # code...
        $invoice_header = InvoiceHeader::getBySlug($invoice_slug);
        if ($invoice_header != NULL or !empty($invoice_header)) {
            $version_string[0] = 'Original for Buyer';
            return view('preview', compact('invoice_header', 'version_string'));
        } else {
            abort(404, "Invoice you are trying to view was not found in our records.");
        }
    }
    
    /**
     * Display a print view of the given invoice ID.
     *
     * @return \Illuminate\Http\Response
     */
    public function printinvoice(Request $request, $invoice_slug='') {
        # code...
        $invoice_header = InvoiceHeader::getBySlug($invoice_slug);
        if ($invoice_header != NULL or !empty($invoice_header)) {
            $version_string[0] = "Original for Buyer";
            $version_string[1] = "Duplicate for Transport";
            $version_string[2] = "Triplicate for Seller";
            return view('print', compact('invoice_header', 'version_string'));
        } else {
            abort(404, "Invoice you are trying to view was not found in our records.");
        }
    }
    
    
    
    /**
     * Save the sales invoice.
     *
     * @return \Illuminate\Http\Response
     */
    public function save_invoice(SalesInvoiceSave $request) {
        # code...
        try {
            //remove the buyer stored in session so it does not appear twice in next invoice screen.
            if (Session::has('buyer'))
                Session::forget('buyer');
            if (Session::has('invoice_lines_count'))
                Session::forget('invoice_lines_count');
            if (Session::has("transport"))
                Session::forget("transport");
            
            //if everything is ok
            if ($request->has('invoiceSlug')) {
                $slug = $request->get('invoiceSlug');
            } else {
                $slug = str_slug(Auth::user()->id." ".$request->get('invoice_number')." ".$request->get('invoice_date')." ".$request->get('buyer_id'), "-");
            }
            
            DB::beginTransaction();
            
            //pick the transport and check whether it is new one or exisitng.
            if (!is_null($request->get('transport')) && !is_numeric($request->get('transport'))) {
                $params = [
                    "transport_name"    => $request->get('transport'),
                    "created_at"        => Carbon::now(),
                    "updated_at"        => Carbon::now()
                ];
                $record = Transport::create($params);
                $transport_id = $record["id"];
                // var_dump ($record);
                // echo "<br><br>";
                // var_dump ($transport_id);
            } else {
                $transport_id = $request->get("transport");
            }
            
            //pick the buyer and check whether he is new one or exisitng.
            if (!is_numeric($request->get('buyer_id'))) {
                $params = [
                    "name"       => $request->get('buyer_id'),
                    "address"    => $request->get('address'),
                    "address_2"  => $request->get('address_2'),
                    "mobile"     => $request->get('mobile'),
                    "gstn"       => $request->get('gstn'),
                    "state"      => $request->get('state'),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ];
                $record = Buyer::create($params);
                $buyer_id = $record["id"];
            } else {
                $buyer_id = $request->get('buyer_id');
            }

            //convert invoice_date to MySql format
            $date = \DateTime::createFromFormat('d/m/Y', $request->get('invoice_date'));
            $invoice_date = $date->format('Y-m-d');
            
            //add invoice header to the db table.
            $params = [
                "buyer_id"            => $buyer_id,
                "user_id"             => Auth::user()->id,
                "invoice_number"      => $request->get('invoice_number'),
                "invoice_date"        => $invoice_date,
                "transport_id"        => $transport_id,
                "challan_number"      => $request->get('challan_number'),
                "invoice_total"       => $request->get('invoice_total'),
                "cgst_percent"        => $request->get('cgst_percent'),
                "cgst_amount"         => $request->get('cgst_amount'),
                "sgst_percent"        => $request->get('sgst_percent'),
                "sgst_amount"         => $request->get('sgst_amount'),
                "igst_percent"        => $request->get('igst_percent'),
                "igst_amount"         => $request->get('igst_amount'),
                "round_off"           => $request->get('round_off'),
                "grand_total"         => $request->get('grand_total'),
                "amount_in_words"     => $request->get('amount_in_words'),
                "slug"                => $slug,
                "created_at"          => Carbon::now(),
                "updated_at"          => Carbon::now()
            ];

            $record = InvoiceHeader::updateOrCreate(['slug' => $slug], $params);
            $new_invoice_number = $record['invoice_number'];

            InvoiceLines::where("invoice_header_id", $record->id)->delete();

            //deal with invoice line
            $invoice_id  = $record['id'];
            $description = $request->get('description');
            $quantity    = $request->get('qty');
            $unit        = $request->get('unit');
            $rate        = $request->get('rate');
            $discount    = $request->get('discount');
            $amount      = $request->get('amt');
            $hsn_code    = $request->get('hsn_code');
            
            foreach($description as $key => $value) {
                
                if (!empty($value) && !is_numeric($value)) {
                    $hsn = $hsn_code[$key];
                    
                    $params = [
                        "description"  =>  $value,
                        "hsn_code"     =>  $hsn,
                        "created_at"   =>  Carbon::now(),
                        "updated_at"   =>  Carbon::now()
                    ];
                    
                    $record = Item::create($params);
                    $item_id = $record['id'];
                } else {
                    $item_id = $value;
                }
                
                $qty     = $quantity[$key];
                $unt     = $unit[$key];
                $rt      = $rate[$key];
                $dis     = $discount[$key];
                $amt     = $amount[$key];
                
                $params  = [
                    'invoice_header_id'  => $invoice_id,
                    'item_id'            => $item_id,
                    'quantity'           => $qty,
                    'unit_id'            => $unt,
                    'rate'               => $rt,
                    'discount'           => $dis,
                    'amount'             => $amt,
                    'created_at'         => Carbon::now(),
                    'updated_at'         => Carbon::now()
                ];
                
                InvoiceLines::create($params);
            }
            
            DB::commit();
            
            if ($request->get('buttonType') == 'save_print') {
                $print_url = route('print', ['invoice_slug' => $slug]);
                return view('printbridge')->with("print_url", $print_url);
            } else if ($request->get('buttonType') == 'save_preview') {
                $print_url = route('printview', ['invoice_slug' => $slug]);
                return view('printbridge')->with("print_url", $print_url);
            } else if ($request->get('buttonType') == 'save') {
                return redirect()->back()->with("type", "success")->with("message", "Invoice with invoice number ".$new_invoice_number." created successfully. <a href='".route('printview', ['invoice_slug' => $slug])."' target='_blank'>Click here</a> to preview it.");
            }
        } catch (Exception $e) {
            DB::rollback();
        }
    }
    
    public function sort_table (Request $request) {
        $column       = $request->get('column') ? $request->get('column') : "invoice_number";
        $orderby      = $request->get('orderby') ? $request->get('orderby') : "desc";
        $no_of_record = $request->get('no_of_record');
        $search       = $request->get('search');
        $user         = User::where("id", Auth::user()->id)->first();    
        $msg          = $user["name"];
        $invoices     = InvoiceHeader::filter($column, $orderby, $no_of_record, $search);
        $dashboard    = view("layouts.dashboard", compact("invoices"))->render();
        $response     = array("msg" => "success", "html" => $dashboard);
        return response()->json($response);
    }

    public function download (Request $request) {
        
        $start_date = $request->get('invoice_start_date');
        $end_date   = $request->get('invoice_end_date');
            
        if ($request->get('excel') == 'true' && $request->get('pdf') == 'true') {
            
            $excel_file = Helper::exportInvoiceTableData($start_date, $end_date, 'xlsx');
            $pdf_file   = Helper::exportInvoiceTableData($start_date, $end_date, 'pdf');
            
            $files = glob(storage_path('exports'));
            $zipFileName = storage_path('zip/export_reports.zip');
            Zipper::make($zipFileName)->add($files)->close();
            
            File::delete(File::glob(storage_path('exports/*')));
            
            return response()->download($zipFileName)->deleteFileAfterSend(true);
        }
        
        else if ($request->get('excel') == 'true') {
            
            $excel_file = Helper::exportInvoiceTableData($start_date, $end_date, 'xlsx');
            return response()->download($excel_file)->deleteFileAfterSend(true);
        }
        
        else if ($request->get('pdf') == 'true') {
            
            $pdf_file   = Helper::exportInvoiceTableData($start_date, $end_date, 'pdf');
            return response()->download($pdf_file)->deleteFileAfterSend(true);
        }
    }
    
    public function save_form_data (Request $request) {
        $cookie = Cookie::make('sales_form_new_buyer', $request->get("buyer").";".$request->get("gstn").";".$request->get("address").";".$request->get("address_2").";".
                                                  $request->get("mobile").";".$request->get("state")
                                                  );
        Session::put("cookie_name", 'sales_form_new_buyer');
        return response()->json(["cookie_name" => 'sales_form_new_buyer'])->withCookie($cookie);
    }
    
    public function save_single_record(Request $request) {
        if ($request->get("form") == "buyer" && $request->get("type") == "update-buyer") {
            
            $buyer = Buyer::whereId($request->get("id"))->update([
                        "name"          => $request->get("name"),
                        "address"       => $request->get("address"),
                        "address_2"     => $request->get("address_2"),
                        "mobile"        => $request->get("mobile"),
                        "email_address" => $request->get("email"),
                        "gstn"          => $request->get("gstn"),
                        "city"          => $request->get("city"),
                        "state"         => $request->get("state"),
                        "status"        => $request->get("status"),
                        "updated_at"    => Carbon::now()
                    ]);
        
            return response()->json(["type" => "success", "msg" => "Buyer updated successfully"]);
        } else if ($request->get("form") == "buyer" && $request->get("type") == "add-buyer") {
            
            Buyer::insert([
                    "name"          => $request->get("name"),
                    "address"       => $request->get("address"),
                    "address_2"     => $request->get("address_2"),
                    "mobile"        => $request->get("mobile"),
                    "email_address" => $request->get("email"),
                    "gstn"          => $request->get("gstn"),
                    "city"          => $request->get("city"),
                    "state"         => $request->get("state"),
                    "status"        => $request->get("status"),
                    "created_at"    => Carbon::now(),
                    "updated_at"    => Carbon::now()
                ]);
            
            $buyers = Buyer::filter();
            $buyer_dashbooard = view("layouts.buyers", compact("buyers"))->render();
            return response()->json(["type" => "success", "msg" => "Buyer added successfully", "html" => $buyer_dashbooard]);
        
            
        } else if ($request->get("form") == "item" && $request->get("type") == "add-item") {
            
            Item::insert([
                    "description"   => $request->get("description"),
                    "hsn_code"      => $request->get("hsn_code"),
                    "status"        => $request->get("status"),
                    "created_at"    => Carbon::now(),
                    "updated_at"    => Carbon::now()
                ]);
            
            $items = Item::filter();
            $item_dashbooard = view("layouts.items", compact("items"))->render();
            return response()->json(["type" => "success", "msg" => "Item added successfully", "html" => $item_dashbooard]);
            
        } else if ($request->get("form") == "item" && $request->get("type") == "update-item") {
            
            $buyer = Item::whereId($request->get("id"))->update([
                        "description"   => $request->get("description"),
                        "hsn_code"      => $request->get("hsn_code"),
                        "status"        => $request->get("status"),
                        "updated_at"    => Carbon::now()
                    ]);
        
            return response()->json(["type" => "success", "msg" => "Item updated successfully"]);
            
        } else if ($request->get("form") == "unit" && $request->get("type") == "add-unit") {
            
            Unit::insert([
                    "name"          => $request->get("name"),
                    "short_name"    => $request->get("short_name"),
                    "sort_order"    => $request->get("sort_order"),
                    "status"        => $request->get("status"),
                    "created_at"    => Carbon::now(),
                    "updated_at"    => Carbon::now()
                ]);
            
            $units = Unit::filter();
            $unit_dashbooard = view("layouts.units", compact("units"))->render();
            return response()->json(["type" => "success", "msg" => "Unit added successfully", "html" => $unit_dashbooard]);
            
        } else if ($request->get("form") == "unit" && $request->get("type") == "update-unit") {
            
            $buyer = Unit::whereId($request->get("id"))->update([
                        "name"        => $request->get("name"),
                        "short_name"  => $request->get("short_name"),
                        "sort_order"  => $request->get("sort_order"),
                        "status"      => $request->get("status"),
                        "updated_at"  => Carbon::now()
                    ]);
        
            return response()->json(["type" => "success", "msg" => "Unit updated successfully"]);
            
        } else {
            return response()->json(["type" => "failure", "msg" => "Something went wrong"]);
        }
    }
    
    public function delete_single_record (Request $request) {
        if ($request->get("form") == "buyer") {
            Buyer::find($request->get("id"))->delete();
            return response()->json(["type" => "success", "msg" => "Buyer deleted successfully."]);
        }
        else if ($request->get("form") == "item") {
            Item::find($request->get("id"))->delete();
            return response()->json(["type" => "success", "msg" => "Item deleted successfully."]);
        }
        else if ($request->get("form") == "unit") {
            Unit::find($request->get("id"))->delete();
            return response()->json(["type" => "success", "msg" => "Unit deleted successfully."]);
        }
    }
    
    public function bulk_import (Request $request) {
        if ($request->get("type") == "bulk-upload" && $request->get("form") == "buyer") {
            
            $file = $request->file("buyer_file")->getClientOriginalName();
            $request->file("buyer_file")->move(storage_path("imported"), $request->file("buyer_file")->getClientOriginalName());
            
            Excel::filter('chunk')->load(storage_path("imported/".$file))->chunk(250, function($results)
            {
                $data = [];
                $cnt = 0;
                foreach($results as $row)
                {
                    if (!empty($row)) {
                        $row->each(function($cells) use (&$data, &$cnt) {
                            $temp = [];
                            $cells->each(function($cell, $col) use (&$temp) {
                               $temp[$col] = $cell;
                            });
                            $data[$cnt++] = $temp;
                        });
                    }
                }
                // var_dump($data);
                Buyer::insert($data);
            });
            return redirect()->to("buyers");
        } else if ($request->get("type") == "bulk-upload" && $request->get("form") == "item") {
            
            $file = $request->file("item_file")->getClientOriginalName();
            $request->file("item_file")->move(storage_path("imported"), $request->file("item_file")->getClientOriginalName());
            
            Excel::filter('chunk')->load(storage_path("imported/".$file))->chunk(250, function($results)
            {
                $data = [];
                $cnt = 0;
                foreach($results as $row)
                {
                    if (!empty($row)) {
                        $row->each(function($cells) use (&$data, &$cnt) {
                            $temp = [];
                            $cells->each(function($cell, $col) use (&$temp) {
                               $temp[$col] = $cell;
                            });
                            $data[$cnt++] = $temp;
                        });
                    }
                }
                Item::insert($data);
            });
            return redirect()->to("items");
        } else {
            echo "some issue";
            //return redirect("home");
        }
    }
}