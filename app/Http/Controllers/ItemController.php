<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Response;

use App\Item;
use App\User;



class ItemController extends Controller
{
    //
    public function items (Request $request) {
        //serve all the items in the table.
        $items = Item::filter();
        $item_dashboard = view("layouts.items", compact("items"));
        return view ("items", compact("item_dashboard"));
    }
    
    public function sort_item (Request $request) {
        $column           = $request->get('column') ? $request->get('column') : "id";
        $orderby          = $request->get('orderby') ? $request->get('orderby') : "desc";
        $no_of_record     = $request->get('no_of_record');
        $search           = $request->get('search');
        $user             = User::where("id", Auth::user()->id)->first();
        $msg              = $user["name"];
        $items            = Item::filter($column, $orderby, $no_of_record, $search);
        $item_dashboard   = view("layouts.items", compact("items"))->render();
        $response         = array("msg" => "success", "html" => $item_dashboard);
        return response()->json($response);
    }
    
    public function download_bulk_import_item_template () {
        return Response::download(storage_path('bulk_import/item_upload_billing.xls'));
    }
}
