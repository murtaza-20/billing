<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Unit;
use App\User;

use Auth;

class UnitController extends Controller
{
    //
    public function units (Request $request) {
        //serve all the items in the table.
        $units = Unit::filter();
        $unit_dashboard = view("layouts.units", compact("units"));
        return view ("units", compact("unit_dashboard"));
    }
    
    public function sort_unit (Request $request) {
        $column           = $request->get('column') ? $request->get('column') : "id";
        $orderby          = $request->get('orderby') ? $request->get('orderby') : "desc";
        $no_of_record     = $request->get('no_of_record');
        $search           = $request->get('search');
        $user             = User::where("id", Auth::user()->id)->first();
        $msg              = $user["name"];
        $units            = Unit::filter($column, $orderby, $no_of_record, $search);
        $unit_dashboard   = view("layouts.units", compact("units"))->render();
        $response         = array("msg" => "success", "html" => $unit_dashboard);
        return response()->json($response);
    }
}
