<?php
namespace App\Http\Helper;

use App\InvoiceHeader;

use Excel;
use Zipper;

class Helper {
    
    public static $includeInvoiceLines = false;
    public static $headers = [
            "Invoice Number",
            "Invoice Date",
            "Buyer",
            "Transport",
            "Challan Number",
            "CGST Percent",
            "CGST Amount",
            "SGST Percent",
            "SGST Amount",
            "IGST Percent",
            "IGST Amount",
            "Invoice Total",
            "Grand Total"
        ];
    
    public static function exportInvoiceTableData ($start_date, $end_date, $fileType) {
        $invoice_start_date   = self::mySqlDateConvertor('d/m/Y', 'Y-m-d', $start_date);
        $invoice_end_date     = self::mySqlDateConvertor('d/m/Y', 'Y-m-d', $end_date);
        
        $invoices = InvoiceHeader::whereBetween("invoice_date", [$invoice_start_date, $invoice_end_date])->get();
    
        $fileName = 'invoices_from_'.$invoice_start_date.'_to_'.$invoice_end_date;
    
        Excel::create($fileName, function($excel) use ($invoices) {
            $excel->sheet('invoices', function($sheet) use ($invoices) {
                $sheet->row(1, self::$headers);
                
                $sheet->cells('A1:M1', function($cells) {
                    $cells->setBackground('#DFDFDF');
                    $cells->setFontColor('#000000');
                    $cells->setFontSize(12);
                    $cells->setAlignment('center');
                    $cells->setValignment('center');
                    
                });
                $sheet->setBorder('A1:M1','thick');                
                $sheet->getRowDimension(1)->setRowHeight(20);
                
                foreach ($invoices as $key => $invoice) {
                    $temp = [
                        $invoice["invoice_number"],
                        self::mySqlDateConvertor('Y-m-d', 'd/m/Y', $invoice['invoice_date']),
                        $invoice->buyer->name,
                        $invoice["transport"],
                        $invoice["challan_number"],
                        sprintf("%0.2f%%", $invoice["cgst_percent"]),
                        sprintf("%0.2f  ", $invoice["cgst_amount"]),
                        sprintf("%0.2f%%", $invoice["sgst_percent"]),
                        sprintf("%0.2f  ", $invoice["sgst_amount"]),
                        sprintf("%0.2f%%", $invoice["igst_percent"]),
                        sprintf("%0.2f  ", $invoice["igst_amount"]),
                        sprintf("%0.2f  ", $invoice["invoice_total"]),
                        sprintf("%0.2f  ", $invoice["grand_total"])
                    ];
                    
                    $sheet->row($key+2, $temp);
                    $sheet->getRowDimension($key+2)->setRowHeight(15);
                    $sheet->cells('A'.($key+2).':M'.($key+2), function($cells) {
                        $cells->setFontSize(10);
                        $cells->setValignment('center');
                    });
                    
                    if (($key+2)%2 != 0) {
                        $sheet->cells('A'.($key+2).':M'.($key+2), function($cells) {
                            $cells->setBackground('#e6f1ec');
                        });
                    }
                    
                    $sheet->setBorder('A'.($key+2).':M'.($key+2),'solid');
                    
                    $sheet->cell('A'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('B'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('C'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('D'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('E'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('F'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('G'.($key+2), function($cell){
                        $cell->setAlignment('right');
                    });
                    $sheet->cell('H'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('I'.($key+2), function($cell){
                        $cell->setAlignment('right');
                    });
                    $sheet->cell('J'.($key+2), function($cell){
                        $cell->setAlignment('center');
                    });
                    $sheet->cell('K'.($key+2), function($cell){
                        $cell->setAlignment('right');
                    });
                    $sheet->cell('L'.($key+2), function($cell){
                        $cell->setAlignment('right');
                    });
                    $sheet->cell('M'.($key+2), function($cell){
                        $cell->setAlignment('right');
                    });
                    $temp = [];
                }
                
            });
        })->store($fileType, storage_path('exports'));
        
        return storage_path('exports/'.$fileName.'.'.$fileType);
    }
    
    /*
    * Convert date to MySql Date format
    * return @string
    * */
    public static function mySqlDateConvertor ($format, $required, $date) {
        //convert invoice_start_date to MySql format
        $date = \DateTime::createFromFormat($format, $date);
        return $date->format($required);
    }

}

