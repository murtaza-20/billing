<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class InvoiceLines extends Model
{
    //
	protected $table = "invoice_lines";
	protected $primaryKey = "id";
	protected $fillable = ["invoice_header_id", "item_id", "quantity", "unit_id", "rate", "discount", "amount", "created_at", "updated_at"];
	
	public function invoice_header() {
	    return $this->belongsTo('App\InvoiceHeader');
	}
	
	public function item() {
	    return $this->hasOne ('App\Item', 'id', 'item_id');
	}
	
	public function unit() {
		return $this->hasOne ('App\Unit', 'id', 'unit_id');
	}
}
