<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth', 'trim']], function(){
	
	//Home Controllers
	Route::get('/sales/{invoiceSlug?}', 'HomeController@sales')->name('sales');
	Route::get('/printview/{invoice_slug?}', 'HomeController@printview')->name('printview');
	Route::get('/print/{invoice_slug?}', 'HomeController@printinvoice')->name('print');
	Route::post('/save_invoice', 'HomeController@save_invoice')->name('save_invoice');
	Route::get('/printbridge', 'HomeController@printbridge')->name('printbridge');
	
	Route::post('/sort_table', 'HomeController@sort_table')->name('sort_table');
	Route::get('/download', 'HomeController@download')->name('download');
	Route::get("/save_form_data", "HomeController@save_form_data")->name("save_form_data");
	
	Route::post("/save_single_record", "HomeController@save_single_record")->name("save_single_record");
	Route::post("/delete_single_record", "HomeController@delete_single_record")->name("delete_single_record");
	Route::post("/bulk_import", "HomeController@bulk_import")->name("bulk_import");
	
	
	//Buyer Controllers
	Route::get("/buyers", "BuyerController@buyers")->name("buyers");
	Route::post("/sort_buyer", "BuyerController@sort_buyer")->name("sort_buyer");
	Route::get("/download_bulk_import_buyer_template", "BuyerController@download_bulk_import_buyer_template")->name("download_bulk_import_buyer_template");
	
	
	//Item Controllers
	Route::get("/items", "ItemController@items")->name("items");
	Route::post("/sort_item", "ItemController@sort_item")->name("sort_item");
	Route::get("/download_bulk_import_item_template", "ItemController@download_bulk_import_item_template")->name("download_bulk_import_item_template");
	
	
	//Unit Controllers
	Route::get("/units", "UnitController@units")->name("units");
	Route::post("/sort_unit", "UnitController@sort_unit")->name("sort_unit");
});